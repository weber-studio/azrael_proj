using System;
using Framework.Tools;
using UnityEngine;
using UnityEngine.UI;

public class testSrc : MonoBehaviour
{
	public Button Btn;

	void Start()
	{
		Btn.onClick.AddListener(() =>
		{
			Debug.Log($"Application.dataPath:{Application.dataPath}");
			LoadAssetBundleTestMethod();
		});

	}


	private void LoadAssetBundleTestMethod()
	{
		/// 单例的AB加载类的加载AB包资源方式
		// AssetBundleLoader.Instance.SetDir(Application.streamingAssetsPath + "/AssetBundles/cube.ab");
		// var ab = AssetBundleLoader.Instance.LoadFromFileAsync();
		// var cube = ab.LoadAsset<GameObject>("AB_Cube");
		// GameObject.Instantiate(cube);

		/// 带释放的AB包同步加载方式
		using (AssetBundleLoaderII loader = new AssetBundleLoaderII("cube.ab"))
		{
			var obj = loader.LoadAsset<GameObject>("AB_Cube");
			if (obj == null)
			{
				Debug.LogError($"获取失败，obj为空！");
				return;
			}
			Instantiate(obj);

		}
	}

	// public async Task button1_Click()
	// {
	// 	Btn.enabled = false;
	// 	var str = "";
	// 	for (int i = 0; i < 5; i++)
	// 	{
	// 		str += await doSomething(i);
	// 	}
	// 	Debug.Log(str);
	// 	Btn.enabled = true;
	// }

	// Task<int> doSomething(int count)
	// {
	// 	return Task.Run(() =>
	// 	{
	// 		Thread.Sleep(1000);
	// 		return count;
	// 	});
	// }


	// private bool moveDirection = true;
	// private bool scaleDirection = true;
	// private Renderer rend;
	//
	// void Start()
	// {
	// 	rend = this.GetComponent<Renderer>();
	// 	// StartCoroutine(ChangeColor());
	// }
	//
	// void Update()
	// {
	// 	this.transform.position += Vector3.right * Time.deltaTime  * (moveDirection == true ? 1.0f : -1.0f);
	// 	if (this.transform.position.x > 4.0f || this.transform.position.x < 0.0f)
	// 	{
	// 		moveDirection = !moveDirection;
	// 	}
	//
	// 	this.transform.localScale += Vector3.one * 0.01f* (scaleDirection == true ? 1.0f : -1.0f);
	// 	if (this.transform.localScale.x > 2.0f || this.transform.localScale.x < 1.0f)
	// 	{
	// 		scaleDirection = !scaleDirection;
	// 	}
	// }
	//
	// void FixedUpdate()
	// {
	// 	this.transform.rotation = Quaternion.Euler(Vector3.one * 45.0f * Time.fixedDeltaTime) * this.transform.rotation;
	// }
	//
	// IEnumerator ChangeColor()
	// {
	// 	while (true)
	// 	{
	// 		rend.material.color = Random.ColorHSV();
	// 		yield return 0;
	// 	}
	// }
	//
	// void OnGUI()
	// {
	// 	GUI.color = Color.red;
	//
	// 	GUIStyle buttonStyle = new GUIStyle(GUI.skin.button);
	// 	buttonStyle.fontSize = 30;
	//
	// 	GUIStyle labelStyle = new GUIStyle(GUI.skin.label);
	// 	labelStyle.fontSize = 30;
	//
	// 	GUILayoutOption[] options = new GUILayoutOption[] { GUILayout.Width(300), GUILayout.Height(100) };
	//
	// 	GUILayout.BeginHorizontal();
	// 	if (GUILayout.Button("TimeScale = 0", buttonStyle, options) == true)
	// 	{
	// 		Time.timeScale = 0;
	// 	}
	// 	if (GUILayout.Button("TimeScale = 0.5", buttonStyle, options) == true)
	// 	{
	// 		Time.timeScale = 0.5f;
	// 	}
	// 	if (GUILayout.Button("TimeScale = 1", buttonStyle, options) == true)
	// 	{
	// 		Time.timeScale = 1;
	// 	}
	// 	if (GUILayout.Button("TimeScale = 2", buttonStyle, options) == true)
	// 	{
	// 		Time.timeScale = 2;
	// 	}
	// 	GUILayout.EndHorizontal();
	//
	// 	GUILayout.Space(50);
	// 	GUILayout.Label("Time.timeScale : " + Time.timeScale, labelStyle);
	// 	GUILayout.Space(50);
	// 	GUILayout.Label("Time.realtimeSinceStartup : " + Time.realtimeSinceStartup, labelStyle);
	// 	GUILayout.Space(50);
	// 	GUILayout.Label("Time.time : " + Time.time, labelStyle);
	// 	GUILayout.Label("Time.unscaledTime : " + Time.unscaledTime, labelStyle);
	// 	GUILayout.Space(50);
	// 	GUILayout.Label("Time.deltaTime : " + Time.deltaTime, labelStyle);
	// 	GUILayout.Label("Time.unscaledDeltaTime : " + Time.unscaledDeltaTime, labelStyle);
	// 	GUILayout.Space(50);
	// 	GUILayout.Label("Time.fixedTime : " + Time.fixedTime, labelStyle);
	// 	GUILayout.Label("Time.fixedUnscaledTime : " + Time.fixedUnscaledTime, labelStyle);
	// 	GUILayout.Space(50);
	// 	GUILayout.Label("Time.fixedDeltaTime : " + Time.fixedDeltaTime, labelStyle);
	// 	GUILayout.Label("Time.fixedUnscaledDeltaTime : " + Time.fixedUnscaledDeltaTime, labelStyle);
	// }
}

public abstract class TestClass
{
	protected static string Name;

	protected abstract void Method();

	protected virtual void Method2()
	{
		Debug.Log($"This is base Method2");
	}
}

/// <summary>
/// 演示释放托管资源与非托管资源的方式
/// </summary>
public class TTT : TestClass, IDisposable
{
	private IntPtr m_Handle;    // 非托管资源
	private System.ComponentModel.Component m_Comp; // 托管资源
	private bool m_Disposed;

	public TTT()
	{

	}

	protected override void Method()
	{

	}

	protected override void Method2()
	{
		base.Method2();
		Debug.Log($"This is override Method2");
	}

	public void Method3()
	{

	}

	public void Dispose()
	{
		Dispose(true);
		GC.SuppressFinalize(this);
		Debug.Log($"1919810");
	}

	// 子类若有自己的非托管资源则可以重写该方法，添加自己的非托管资源的释放
	// 但重写的时候要保证调用基类的该方法，以确保基类的非托管资源能够正常释放
	protected virtual void Dispose(bool disposing)
	{
		if (!this.m_Disposed)
		{
			if (disposing)
			{
				// 释放托管资源
				m_Comp.Dispose();
			}
			// 释放非托管资源
			// closeHandle(m_Handle); 该方法是例子，具体如何释放非托管资源要按照类型决定
			// ToDo: (该todo是一个案例并非待做任务) 释放非托管资源，并设置变量为null(下边的例子就是将handle置null的操作)
			m_Handle = IntPtr.Zero;
		}

		this.m_Disposed = true;
	}

	~TTT()
	{
		Dispose(false);
	}
}
