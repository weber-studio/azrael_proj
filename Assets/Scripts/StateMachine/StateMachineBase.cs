using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 状态机基类
/// </summary>
public abstract class StateMachineBase
{
    protected IState curState;

    public virtual void ChangeState(IState newState)
    {
        if (newState == null)
        {
            Debug.LogError($"传入的新状态为空！");
            return;
        }

        curState?.OnExit();

        curState = newState;

        curState.OnEnter();
    }

    public virtual void HandleInput()
    {
        curState?.HandleInput();
    }
    
    public virtual void Update()
    {
        curState?.Update();
    }
    
    public virtual void PhysicsUpdate()
    {
        curState?.PhysicsUpdate();
    }
}
