using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 状态机的状态接口，规范实现
/// </summary>
public interface IState
{
    void OnEnter();

    void HandleInput();

    void Update();

    void PhysicsUpdate();

    void OnExit();
}
