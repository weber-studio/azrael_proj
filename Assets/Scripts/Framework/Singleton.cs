using System;
using System.Linq;
using UnityEngine;

namespace Framework
{
    // 饿汉式-主动加载
    public class Singleton<T> where T : Singleton<T>, new()
    {
        // 防止外部创造实例破坏唯一性，继承该单例模式的类也应该将构造函数私有化
        protected Singleton() { }

        public static T Instance { get; private set; } 

        // 静态构造函数，在第一次引用静态成员的时候提前调用 => 即第一次访问单例的Instance时会先调用静态构造函数
        static Singleton()
        {
            Debug.Log($"静态构造函数。");
            if (Instance == null)
            {
                // 该方法创建实例最快
                Instance = Activator.CreateInstance(typeof(T), true) as T;
                Instance.Init();
                // Instance = Activator.CreateInstance<T>(); // 无参构造
            }
        }

        protected virtual void Init()
        {

        }
    }

    // 懒人式-按需加载-需要考虑线程安全-上锁
    public class LazySingleton<T> where T : LazySingleton<T>
    {
        protected LazySingleton() { }

        private volatile static T m_Instance;

        private static object m_Locker = new object();

        public static T Instance
        {
            get
            {
                if (m_Instance == null) 
                {
                    lock (m_Locker)
                    {
                        if (m_Instance == null)
                        {
                            m_Instance = Activator.CreateInstance(typeof(T), true) as T;
                            m_Instance.Init();
                        }
                    }
                }

                return m_Instance;
            }
        }

        protected virtual void Init()
        {

        }
    }

    // 继承mono的单例，原理与懒汉式类似，按需加载
    // 应该需要考虑线程安全
    public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
    {
        private static T m_Instance;

        public static T Instance
        {
            get
            {
                if (m_Instance != null) return m_Instance;

                m_Instance = FindObjectOfType<T>();

                if (m_Instance == null)
                {
                    var name = typeof(T).ToString().Split('.').Last();
                    new GameObject($"Singleton of {name}").AddComponent<T>();
                }
                else
                {
                    m_Instance.Init();
                }

                return m_Instance;
            }
        }

        private void Awake()
        {
            m_Instance = this as T;
            Init();
        }

        protected virtual void Init()
        {

        }
    }

}
