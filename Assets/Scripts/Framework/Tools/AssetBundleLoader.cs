using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Networking;

namespace Framework.Tools
{
    public sealed class AssetBundleLoader : Singleton<AssetBundleLoader>
    {
        /// <summary>
        /// 已经加载的ab包的缓存，防止重复加载
        /// 使用过后应该及时卸载
        /// </summary>
        protected Dictionary<string, AssetBundle> m_AssetBundlesDic;

        /// <summary>
        /// 不使用协程的异步加载本地AB包的默认方式，没有返回值且不需要使用async修饰，传入委托完成
        /// </summary>
        /// <param name="path"></param>
        /// <param name="bundleName"></param>
        /// <param name="onComplete">传入的回调要判断加载的ab包是否加载成功</param>
        /// <param name="autoUnload"></param>
        public void LoadAssetBundle(string path, string bundleName, Action<AssetBundle> callBack, bool autoUnload = true)
        {
            if (CheckDir(path) == false)
            {
                return;
            }
            var dir = path + "/" + "bundleName";

            var request = AssetBundle.LoadFromFileAsync(dir);
            request.completed += (AsyncOperation asset) =>
            {
                var request = (AssetBundleCreateRequest) asset;
                if (request == null)
                {
                    Debug.LogError($"AB包\"{bundleName}\"加载失败");
                    return;
                }

                Debug.Log($"AB包\"{bundleName}\"加载成功");

                var bundle = request.assetBundle;
                callBack?.Invoke(bundle);

                if (autoUnload)
                {
                    bundle.Unload(false);
                    return;
                }

                CacheBundle(bundleName, bundle);
            };
        }

        /// <summary>
        /// 不使用协程的本地异步加载
        /// 该方法借助Cysharp的UniTask完成异步等待的操作
        /// </summary>
        /// <returns></returns>
        public async UniTask<AssetBundle> LoadFromFileAsync(string path, string bundleName)
        {
            Debug.Log($"开始加载AB");
            if (CheckDir(path) == false)
            {
                return null;
            }
            var dir = path + "/" + bundleName;

            // 创建异步加载
            var request = AssetBundle.LoadFromFileAsync(dir);
            // 等待加载
            Debug.Log($"开始异步加载AB包：{dir}，开始时间：{Time.realtimeSinceStartup}");
            var assetBundle = await request;

            if (assetBundle == null)
            {
                Debug.LogError($"加载失败，请检查！");
                return null;
            }

            Debug.Log($"路径：{dir} 下的AB包异步加载成功，AB包名为：{assetBundle.name}，结束时间：{Time.realtimeSinceStartup}！");
            CacheBundle(bundleName, assetBundle);
            var prefab = await assetBundle.LoadAssetAsync<GameObject>("AsunaPrefab");
            return assetBundle;
        }

        public bool CheckDir(string dir)
        {
            if (string.IsNullOrEmpty(dir))
            {
                Debug.LogError($"输入为空，无法修改读取ab包的路径！");
                return false;
            }

            if (Directory.Exists(dir) == false)
            {
                Debug.LogError($"路径不存在！");
                return false;
            }

            return true;
        }

        /// <summary>
        /// 服务器异步加载
        /// </summary>
        /// <returns></returns>
        public async Task<AssetBundle> LoadFromWebAsync(string url)
        {
            if (string.IsNullOrEmpty(url))
            {
                return null;
            }

            var request = UnityWebRequestAssetBundle.GetAssetBundle(url, 3, 0);
            // 等待请求
            await request.SendWebRequest();

            if (request.result != UnityWebRequest.Result.Success)
            {
                Debug.LogError($"下载错误，Error:{request.error}");
                return null;
            }

            var assetBundle = DownloadHandlerAssetBundle.GetContent(request);
            return assetBundle;
        }

        /// <summary>
        /// 缓存已经加载的AB包
        /// </summary>
        /// <param name="name">AB包名</param>
        /// <param name="ab">加载的AB包资源</param>
        /// <returns></returns>
        private bool CacheBundle(string name, AssetBundle ab)
        {
            if (m_AssetBundlesDic == null)
            {
                m_AssetBundlesDic = new Dictionary<string, AssetBundle>();
            }
            
            if (m_AssetBundlesDic.ContainsKey(name))
            {
                Debug.LogWarning($"该Bundle已经缓存过了！");
                return false;
            }
            
            m_AssetBundlesDic.Add(name, ab);
            return true;
        }

        /// <summary>
        /// 根据名字卸载已加载的AB包
        /// </summary>
        /// <param name="name">AB包名</param>
        /// <returns></returns>
        private bool UnloadBundle(string name)
        {
            if (m_AssetBundlesDic == null)
            {
                Debug.LogError($"AB包缓存字典为空！");
                m_AssetBundlesDic = new Dictionary<string, AssetBundle>();
                return false;
            }

            if (!m_AssetBundlesDic.ContainsKey(name))
            {
                Debug.LogWarning($"该key:{name}下的AB包不存在，无需卸载！");
                return false;
            }

            var AB = m_AssetBundlesDic[name];
            AB.Unload(false);
            Debug.Log($"AB卸载成功！");
            return m_AssetBundlesDic.Remove(name);
        }
    }

    /// <summary>
    /// 加载AB资源包
    /// 不认为是一个很好的加载方法
    /// </summary>
    public class AssetBundleLoaderII : IDisposable
    {
        private AssetBundle m_Bundle;

        /// <summary>
        /// 同步加载AB包方法II
        /// 不一定用
        /// </summary>
        /// <param name="path">
        /// 该参数只需要写AssetBundles文件夹为根下的路径就可以了
        /// </param>
        public AssetBundleLoaderII(string path)
        {
            string fullPath = LocalFilesMgr.Instance.m_LocalPath + path;
            m_Bundle = AssetBundle.LoadFromMemory(LocalFilesMgr.Instance.GetBuffer(fullPath));
        }

        public T LoadAsset<T>(string assetName) where T : UnityEngine.Object
        {
            if (m_Bundle == null)
            {
                return default(T);
            }

            T obj = m_Bundle.LoadAsset<T>(assetName);

            if (obj == null)
            {
                Debug.LogError($"从AB包中加载的Asset为空，请检查！");
                return default(T);
            }

            return obj;
        }

        public void Dispose()
        {
            if (m_Bundle != null)
            {
                m_Bundle.Unload(false);
            }
        }
    }

    public class AssetBundleLoaderIIAsync : MonoBehaviour
    {
        private string m_FullPath;
        private string m_Name;

        private AssetBundleCreateRequest m_Request;
        private AssetBundle m_Bunlde;

        public Action<UnityEngine.Object> OnLoadComplete;

        public void Init(string path, string name)
        {
            m_FullPath = LocalFilesMgr.Instance.m_LocalPath + path;
            m_Name = name;
        }

        void Start()
        {
            StartCoroutine(Load());
        }

        private IEnumerator Load()
        {
            m_Request = AssetBundle.LoadFromMemoryAsync(LocalFilesMgr.Instance.GetBuffer(m_FullPath));
            yield return m_Request;
            m_Bunlde = m_Request.assetBundle;
            if (OnLoadComplete != null)
            {
                OnLoadComplete.Invoke(m_Bunlde.LoadAsset(name));
                Destroy(gameObject);
            }
        }

        void OnDestroy()
        {
            if (m_Bunlde != null)
            {
                m_Bunlde.Unload(false);
            }

            m_FullPath = null;
            m_Name = null;
        }
    }
}

