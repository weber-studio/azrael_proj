using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Cysharp.Threading.Tasks;
using UnityEngine;
using UnityEngine.Events;
using Object = UnityEngine.Object;

namespace Framework.Tools
{
    public interface ITestFace
    {

    }

    public class ABMgr : MonoSingleton<ABMgr>
    {
        // 该类目的 => 让外部更方便的对ab包进行资源加载
        // AB包不能重复加载，重复加载会报错
        // 所以需要存储加载过的AB包

        // 加载过且未卸载的包的缓存
        private Dictionary<string, AssetBundle> m_LoadedBundlesDic;
        // 主包的缓存
        private AssetBundle m_MainBundle;
        // 主包的配置文件的缓存
        private AssetBundleManifest m_MainManifest;

        /// <summary>
        /// AB包存放路径，可根据发布平台修改get的返回值
        /// </summary>
        private string m_PathUrl
        {
            get
            {
                return Application.streamingAssetsPath + "/";
            }
        }

        /// <summary>
        /// 主包名，方便修改
        /// </summary>
        private string m_MainAbName
        {
            get
            {
#if UNITY_IOS
                return "IOS";
#elif UNITY_ANDROID
                return "Android";
#else
                return "PC";
#endif
            }
        }


        


        /*----------------------同步加载----------------------*/
        
        /// <summary>
        /// 从本地文件中同步加载AB包并返回
        /// </summary>
        /// <param name="abName">AB包名</param>
        /// <returns></returns>
        public AssetBundle LoadBundleFromFile(string abName)
        {
            // 在加载前先判断是否已经加载过
            // 若已经加载过则直接从字典中获取该bundle
            if (m_LoadedBundlesDic.ContainsKey(abName))
            {
                var bundle = m_LoadedBundlesDic[abName];
                return bundle;
            }

            // 缓存中没有便开始加载
            // 加载AB包流程： 1.加载主包 => 2.加载主包的配置文件 => 3.获取目标包的依赖包相关信息 => 4.加载依赖包 => 5.加载目标包 => 6.缓存到字典以防后续重复加载
            /// 1.加载主包
            if (m_MainBundle == null)
            {
                m_MainBundle = AssetBundle.LoadFromFile(m_PathUrl + m_MainAbName);
            }
            /// 2.加载主包的配置文件
            if (m_MainManifest == null)
            {
                m_MainManifest = m_MainBundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
            }

            /// 3.获取目标包的依赖包的相关信息
            string[] strs = m_MainManifest.GetAllDependencies(abName);

            /// 4.加载依赖包
            for (int i = 0; i < strs.Length; i++)
            {
                var str = strs[i];
                if (!m_LoadedBundlesDic.ContainsKey(str))
                {
                    AssetBundle ab = AssetBundle.LoadFromFile(m_PathUrl + str);
                    m_LoadedBundlesDic.Add(str, ab);
                }
            }

            /// 5.加载目标包
            AssetBundle target = AssetBundle.LoadFromFile(m_PathUrl + abName);

            /// 6.加载完成后缓存到字典
            m_LoadedBundlesDic.Add(abName, target);

            return target;
        }

        /// <summary>
        /// 同步加载AB包中的资源并返回；
        /// 不指定类型，直接返会UnityEngine.Object类型；
        /// </summary>
        /// <param name="abName"></param>
        /// <param name="resName"></param>
        /// <returns></returns>
        public Object LoadRes(string abName, string resName) => LoadRes(abName, resName, typeof(Object));
        // 如果不直接指向的话可以手写下面的方法体，本质是一样的
        // {
        //     AssetBundle target = LoadBundleFromFile(abName);
        //
        //     var res = target.LoadAsset(resName);
        //
        //     // 默认返回Objec类型
        //     return res;
        // }

        /// <summary>
        /// 同步加载AB包中的资源并返回；
        /// 根据泛型指定类型；
        /// 指定类型可避免不同类型的同名资源冲突；
        /// </summary>
        /// <typeparam name="T">T为所需资源对象的类型</typeparam>
        /// <param name="abName">AB包名</param>
        /// <param name="resName">AB包中的资源名</param>
        /// <returns>T类型的资源</returns>
        public T LoadRes<T>(string abName, string resName) where T : Object //=>  (T) LoadRes(abName, resName, typeof(T)); // todo: 测试一下使用 as T 的写法
        // 如果不直接指向的话可以手写下面的方法体，本质是一样的
        {
            // 获取目标AB包
            AssetBundle target = LoadBundleFromFile(abName);
        
            var obj = target.LoadAsset<T>(resName);
        
            return obj;
        }

        /// <summary>
        /// 同步加载AB包中的资源并返回；
        /// 根据type指定类型；
        /// 指定类型可避免不同类型的同名资源冲突；
        /// xLua不支持泛型，可用该方法替代；
        /// </summary>
        /// <param name="abName">AB包名</param>
        /// <param name="resName">AB包中的资源名</param>
        /// <param name="type">资源的类型</param>
        /// <returns>type类型的资源</returns>
        public Object LoadRes(string abName, string resName, System.Type type)
        {
            // 获取目标AB包
            AssetBundle target = LoadBundleFromFile(abName);

            var obj = target.LoadAsset(resName, type);

            // 子类转父类可以隐式转换
            // 父类转子类需要显式强制转换
            return obj;
        }

        public List<T> LoadAllRes<T>(string abName) where T : Object 
        {
            var bundle = LoadBundleFromFile(abName);
            var assets = bundle.LoadAllAssets<T>().ToList();
            return assets;
        }

        public List<Object> LoadAllRes(string abName, System.Type type)
        {
            var bundle = LoadBundleFromFile(abName);
            var assets = bundle.LoadAllAssets(type).ToList();
            return assets;
        }

        public List<Object> LoadAllRes(string abName) => LoadAllRes(abName, typeof(Object));
        




        /*----------------------异步加载----------------------*/
        
        /// <summary>
        /// 本地异步加载AB包。基于CySharp的UniTask包进行可等待的async/await异步操作。
        /// 需要在异步函数(async)中调用使用以实现异步等待(await)。
        /// </summary>
        /// <param name="abName">AB包名</param>
        /// <returns>等待加载完成返回目标AB包</returns>
        public async UniTask<AssetBundle> LoadBundleFromFileAsync(string abName)
        {
            if (m_LoadedBundlesDic.ContainsKey(abName))
            {
                var bundle = m_LoadedBundlesDic[abName];
                return bundle;
            }
        
            if (m_MainBundle == null)
            {
                Debug.Log($"开始异步加载主包！");
                m_MainBundle = await AssetBundle.LoadFromFileAsync(m_PathUrl + m_MainAbName);
                if (m_MainBundle == null)
                {
                    throw new Exception($"主包加载失败！");
                }
            }
            if (m_MainManifest == null)
            {
                Debug.Log($"开始异步加载主包配置文件！");
                m_MainManifest = await m_MainBundle.LoadAssetAsync<AssetBundleManifest>("AssetBundleManifest") as AssetBundleManifest;
                if (m_MainManifest == null)
                {
                    throw new Exception($"主包配置文件加载失败！");
                }
            }
        
            string[] strs = m_MainManifest.GetAllDependencies(abName);
        
            foreach (var str in strs)
            {
                if (!m_LoadedBundlesDic.ContainsKey(str))
                {
                    Debug.Log($"开始加载依赖包：{str}");
                    var dpd = await AssetBundle.LoadFromFileAsync(m_PathUrl + str);
                    if (dpd == null)
                    {
                        throw new Exception($"依赖包“{str}”加载失败！");
                    }
                    m_LoadedBundlesDic.Add(str, dpd);
                }
            }
        
            Debug.Log($"开始加载目标包：{abName}");
            var target = await AssetBundle.LoadFromFileAsync(m_PathUrl + abName);
            if (target == null)
            {
                throw new Exception($"目标包“{abName}”加载失败！");
            }
            m_LoadedBundlesDic.Add(abName, target);
        
            return target;
        }
        
        /// <summary>
        /// 异步加载AB包中资源，不指定类型(一般不建议用)。
        /// 不能像同步加载一样直接指向其他重载函数，因为返回类型为UniTask(Object)，不可以进行UniTask泛型之间的类型转换。
        /// </summary>
        /// <param name="abName"></param>
        /// <param name="resName"></param>
        /// <returns></returns>
        public async UniTask<Object> LoadResAsync(string abName, string resName)
        {
            var bundle = await LoadBundleFromFileAsync(abName);
            var asset = await bundle.LoadAssetAsync(resName);
            return asset;
        }
        
        /// <summary>
        /// 异步加载AB包中资源，传入type指定加载资源的类型。
        /// 不能像同步加载一样直接指向其他重载函数，因为返回类型为UniTask(Object)，不可以进行UniTask泛型之间的类型转换。
        /// </summary>
        /// <param name="abName">AB包名</param>
        /// <param name="resName">资源名</param>
        /// <param name="type">资源类型</param>
        /// <returns>返回Object基类型装在的type类型对象</returns>
        public async UniTask<Object> LoadResAsync(string abName, string resName, System.Type type) 
        {
            var bundle = await LoadBundleFromFileAsync(abName);
            var asset = await bundle.LoadAssetAsync(resName, type);
            return asset;
        }
        
        /// <summary>
        /// 异步加载AB包中资源，根据泛型T指定加载资源的类型。
        /// 不能像同步加载一样直接指向其他重载函数，因为返回类型为UniTask(T)，不可以进行UniTask泛型之间的类型转换。
        /// </summary>
        /// <typeparam name="T">资源类型</typeparam>
        /// <param name="abName">AB包名</param>
        /// <param name="resName">资源名</param>
        /// <returns>返回泛型T对象</returns>
        public async UniTask<T> LoadResAsync<T>(string abName, string resName) where T : Object
        {
            var bundle = await LoadBundleFromFileAsync(abName);
            T asset = (T) await bundle.LoadAssetAsync<T>(resName);
            return asset;
        }
        
        
        /// <summary>
        /// 使用委托的异步加载方式
        /// 提供给外部使用的方法，该方法体主要是启动协程。
        /// </summary>
        /// <param name="abName"></param>
        /// <param name="resName"></param>
        /// <param name="callBack"></param>
        public void LoadResAsyncII(string abName, string resName, UnityAction<Object> callBack) =>
            LoadResAsyncII(abName, resName, typeof(Object), callBack);
        
        public void LoadResAsyncII<T>(string abName, string resName, UnityAction<Object> callBack) where T : Object =>
            LoadResAsyncII(abName, resName, typeof(T), callBack);
        
        public void LoadResAsyncII(string abName, string resName, System.Type type, UnityAction<Object> callBack)
        {
            StartCoroutine(ILoadResAsyncII(abName, resName, type, callBack));
        }
        
        private IEnumerator ILoadResAsyncII(string abName, string resName, System.Type type, UnityAction<Object> callBack)
        {
            var task = LoadBundleFromFileAsync(abName);
            // todo: 需要搞清楚为什么要这样WaitUntil才能正常执行完异步函数。
            yield return new WaitUntil(() => task.GetAwaiter().IsCompleted);
            var bundle = task.GetAwaiter().GetResult();
        
            var request = bundle.LoadAssetAsync(resName, type);
            yield return request;
            
            // 异步加载结束后调用委托并传入刚才加载的资源
            var asset = request.asset;
            
            callBack?.Invoke(asset);
        }

        



        /*----------------------卸载AB包操作----------------------*/

        /// <summary>
        /// 卸载单个AB包
        /// </summary>
        /// <param name="abName">AB包名</param>
        public void UnloadBundle(string abName)
        {
            if (m_LoadedBundlesDic.ContainsKey(abName))
            {
                m_LoadedBundlesDic[abName].Unload(false);
                m_LoadedBundlesDic.Remove(abName);
            }
        }

        /// <summary>
        /// 卸载所有加载过的AB包
        /// </summary>
        public void UnloadAllBundle()
        {
            AssetBundle.UnloadAllAssetBundles(false);
            m_LoadedBundlesDic.Clear();

            // 全部清空之后包括主包也会被清空
            // 所以也要把主包的缓存和主包配置文件的缓存置空保证安全
            m_MainBundle = null;
            m_MainManifest = null;
        }



        /*----------------------初始化重载----------------------*/
        protected override void Init()
        {
            base.Init();
            this.name = $"ABMgr-Singleton";

            if (m_LoadedBundlesDic == null)
            {
                m_LoadedBundlesDic = new Dictionary<string, AssetBundle>();
            }

            m_MainBundle = null;
            m_MainManifest = null;
        }
    }
}
