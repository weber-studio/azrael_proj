using System.IO;
using UnityEngine;

namespace Framework.Tools
{
    public class LocalFilesMgr : Singleton<LocalFilesMgr>
    {
#if UNITY_EDITOR
#if UNITY_STANDALONE_WIN
        public readonly string m_LocalPath = Application.streamingAssetsPath + "/AssetBundles/";
#elif UNITY_ANDROID
        public readonly string m_LocalPath = Application.streamingAssetsPath + "/AssetBundles/Android/";
#elif UNITY_IOS
        public readonly string m_LocalPath = Application.streamingAssetsPath + "/AssetBundles/Ios/";
#endif
#elif UNITY_ANDROID || UNITY_IOS || UNITY_STANDALONE_WIN
        public readonly string m_LocalPath = Application.persistentDataPath + "/";
#endif

        /// <summary>
        /// 读取本地文件到byte数组
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public byte[] GetBuffer(string path)
        {
            byte[] buffer = null;

            Debug.Log($"文件读取的Path:{path}");
            using (FileStream fs = new FileStream(path, FileMode.Open))
            {
                buffer = new byte[fs.Length];
                fs.Read(buffer, 0, buffer.Length);
            }

            return buffer;
        }
    }
}
