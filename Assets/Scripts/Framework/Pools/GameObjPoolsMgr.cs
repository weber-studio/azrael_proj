using System;
using System.Collections.Generic;
using Framework.Extensions;
using UnityEngine;

namespace Framework
{
    /*----------------------游戏对象池管理类----------------------*/

    public sealed class GameObjPoolsMgr : MonoSingleton<GameObjPoolsMgr>
    {
        private Dictionary<string, GameObjPool> m_PoolsDic;

        protected override void Init()
        {
            base.Init();
            transform.position = new Vector3(0, 0, 0);
            m_PoolsDic = new Dictionary<string, GameObjPool>();
        }

        public void RegisterPool(string poolName, GameObject reference, Action<GameObject> init, bool addToDefault = true, int max = 100)
        {
            if (m_PoolsDic.ContainsKey(poolName))
            {
                Debug.Log($"已申请过相同名称“{poolName}”的对象池！");
                return;
            }

            var pool = new GameObjPool(reference, init, poolName, addToDefault, max);
            m_PoolsDic.Add(poolName, pool);
            Debug.Log($"已申请名为“{poolName}”的对象池！");
        }

        public void DeletPool(string poolName)
        {
            if (m_PoolsDic.ContainsKey(poolName))
            {
                var pool = m_PoolsDic[poolName];
                m_PoolsDic.Remove(poolName);
                pool.Clear();
            }
        }

        public GameObjPool GetPool(string name)
        {
            if (!m_PoolsDic.ContainsKey(name))
            {
                Debug.LogError($"不存在名为“{name}”的对象池！");
                return null;
            }

            var pool = m_PoolsDic[name];
            return pool;
        }

        // 一般不会调用到这一部分
        void OnDisable()
        {
            if (m_PoolsDic != null)
            {
                foreach (var kv in m_PoolsDic)
                {
                    kv.Value.Clear();
                }
            }

            this.gameObject.SetActiveWhenNeeded(false);
#if UNITY_EDITOR
            DestroyImmediate(this.gameObject);
#else
            Destroy(this.gameObject);
#endif
        }
    }

}
