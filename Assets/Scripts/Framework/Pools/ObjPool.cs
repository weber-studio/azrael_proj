using System;
using System.Collections.Generic;
using Framework.Extensions;
using UnityEngine;

namespace Framework
{
    /*----------------------对象池类----------------------*/

    public interface IReusable<T>
    {
        public void OnSpawn(T obj);
        public void OnStore(T obj);
    }

    public abstract class Pool<T> : IReusable<T>
    {
        protected List<T> pool;
        protected T reference;

        protected Action<T> initAction;

        protected int maxCount;

        public abstract void Clear();

        /// <summary>
        /// 从对象池中获取对象时使用
        /// </summary>
        public abstract void OnSpawn(T obj);

        /// <summary>
        /// 往对象池中存储对象时使用
        /// </summary>
        public abstract void OnStore(T obj);
    }

    public class ObjPool<T> : Pool<T>
    {

        public ObjPool(T reference, Action<T> init, int max = 100)
        {
            maxCount = max;
            base.reference = reference;
            pool = new List<T>();
            this.initAction = init;
        }

        public override void Clear()
        {
            pool.Clear();
        }

        public override void OnSpawn(T obj)
        {
        }

        public override void OnStore(T obj)
        {
        }
    }

    public class GameObjPool : Pool<GameObject>
    {
        private bool m_AddToDefaultPool;    // 是否加入到场景中的默认对象池容器中，若否则作为对应物体的子物体
        private GameObject m_DefaultPool;   // 场景中的该对象池的元素的默认容器，其父物体为ObjPoolsMgr

        public GameObjPool(GameObject prefab, Action<GameObject> init, string poolName = "Pool", bool AddToDefaultPool = true, int max = 100)
        {
            this.maxCount = max;
            m_AddToDefaultPool = AddToDefaultPool;
            reference = prefab;
            pool = new List<GameObject>();
            initAction = init;

            if (AddToDefaultPool)
            {
                m_DefaultPool = new GameObject(poolName);
                m_DefaultPool.transform.position = new Vector3(0, 0, 0);
                m_DefaultPool.transform.parent = UnityEngine.Object.FindObjectOfType<GameObjPoolsMgr>().transform;
            }
        }

        /// <summary>
        /// 获取对象池的存储集合
        /// 尽量不要使用
        /// </summary>
        /// <returns></returns>
        protected List<GameObject> GetPool()
        {
            if (pool == null)
            {
                Debug.LogError($"对象池为空！");
                return null;
            }

            return pool;
        }

        public void Push(GameObject clone)
        {
            OnStore(clone);

            if (pool.Count >= maxCount)
            {
                Debug.LogError("该对象池已达到上限！");
                GameObject.Destroy(clone);
                return;
            }

            if (pool.Contains(clone))
            {
                // 避免对同一个实例存储多次，出现引用混乱的现象
                Debug.LogError($"池中已有该clone！");
                return;
            }

            pool.Add(clone);
        }

        public GameObject Pop()
        {
            if (pool != null && pool.Count > 0)
            {
                var gObj = pool[0];
                pool.RemoveAt(0);
                OnSpawn(gObj);
                return gObj;
            }

            var newObj = GameObject.Instantiate(reference);
            initAction?.Invoke(newObj);
            OnSpawn(newObj);
            return newObj;
        }

        // 更新对象池的数量，可以用于提前申请
        public void UpdateSize(int size)
        {
            if (size < 0 || size == pool.Count)
            {
                return;
            }

            if (pool.Count > size)
            {
                var temp = pool.Count - size;
                for (int i = 0; i < temp; i++)
                {
                    pool.RemoveAt(pool.Count - 1);
                }
            }
            else if (pool.Count < size)
            {
                if (size > maxCount)
                {
                    Debug.LogError($"新size大于该对象池的maxCount！");
                    return;
                }

                var temp = size - pool.Count;
                for (int i = 0; i < temp; i++)
                {
                    var newObj = GameObject.Instantiate(reference);
                    initAction?.Invoke(newObj);
                    OnSpawn(newObj);

                    newObj.SetActiveWhenNeeded(false);
                    pool.Add(newObj);
                }
            }
        }

        public override void Clear()
        {
            pool.Clear();
        }

        public override void OnSpawn(GameObject obj)
        {
            if (obj.transform.parent == m_DefaultPool.transform)
            {
                return;
            }

            if (m_AddToDefaultPool)
            {
                obj.transform.parent = m_DefaultPool.transform;
            }

            obj.SetActiveWhenNeeded(true);
        }

        public override void OnStore(GameObject obj)
        {
            obj.SetActiveWhenNeeded(false);
        }
    }
}
