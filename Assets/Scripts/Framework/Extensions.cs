using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using UnityEngine;

namespace Framework.Extensions
{
    public static class Extensions
    {
        /// <summary>
        /// GameObject设活的扩展方法，加一层判断方法。
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="active"></param>
        public static void SetActiveWhenNeeded(this GameObject obj, bool active)
        {
            if (obj.activeInHierarchy == active)
            {
                return;
            }
            obj.SetActive(active);
        }

        /// <summary>
        /// 字典取值扩展
        /// </summary>
        /// <typeparam name="TKey">键的类型</typeparam>
        /// <typeparam name="TValue">值的类型</typeparam>
        /// <param name="dic">要取值的字典</param>
        /// <param name="key">键名</param>
        /// <returns></returns>
        public static TValue GetDicValue<TKey, TValue>(this Dictionary<TKey, TValue> dic, TKey key)
        {
            TValue value = default(TValue);
            var result = dic.TryGetValue(key, out value);
            if (result)
            {
                return value;
            }

            Debug.LogError($"对字典“{dic}”获取键为“{key}”的值失败，请检查！");
            return default;
        }

        /// <summary>
        /// 将Unity的异步操作强行转换为TaskAwaiter类型以支持async/await操作。
        /// </summary>
        /// <param name="asyncOperation"></param>
        /// <returns></returns>
        public static TaskAwaiter GetAwaiter(this AsyncOperation asyncOperation) 
        {
            var tcs = new TaskCompletionSource<Object>();
            asyncOperation.completed += obj => { tcs.SetResult(null); };
            return ((Task) tcs.Task).GetAwaiter();
        }

        public static TaskAwaiter GetAwaiter(this WaitForSeconds wait)
        {
            var tcs = new TaskCompletionSource<Object>();
            tcs.SetResult(null);
            return ((Task) tcs.Task).GetAwaiter();
        }
    }
}
