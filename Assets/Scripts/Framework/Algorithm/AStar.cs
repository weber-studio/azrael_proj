using System.Collections.Generic;
using UnityEngine;

namespace Framework.Algorithm
{
    // A星寻路管理器，使用单例
    public class AStarMgr : LazySingleton<AStarMgr>
    {
        private int mapMaxWidth;
        private int mapMaxHeight;

        public AStarNode[,] NodeS
        {
            get;
            private set;
        }

        private List<AStarNode> m_OpenList;
        private List<AStarNode> m_CloseList;

        // 初始化地图以及节点信息
        public void InitMapInfo(int width, int height)
        {
            mapMaxWidth = width;
            mapMaxHeight = height;

            // 初始化节点容器
            NodeS = new AStarNode[width, height];

            // 循环创建节点
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    var x = i;
                    var y = j;
                    Debug.Log($"i:{x} | j:{y}");

                    AStarNode node = new AStarNode(x, y,
                        Random.Range(1, 100) < 20 ? AStarNode.AStarNodeType.Stop : AStarNode.AStarNodeType.Walk);

                    NodeS[x, y] = node;
                }
            }
        }

        /// <summary>
        /// 寻路方法 外部调用
        /// </summary>
        /// <param name="startPos">起点位置</param>
        /// <param name="endPos">终点位置</param>
        /// <returns></returns>
        public List<AStarNode> FindPath(Vector2 startPos, Vector2 endPos)
        {
            // 起点终点是否在范围内 => 判断该点是否合法 => 合法返回对应节点 => 不合法返回null => 清空列表的上一次数据

            // 1.起点终点是否在范围内
            if (startPos.x < 0 || startPos.x >= mapMaxWidth ||
                startPos.y < 0 || startPos.y >= mapMaxHeight ||
                endPos.x < 0 || endPos.x >= mapMaxWidth ||
                endPos.y < 0 || endPos.y >= mapMaxHeight)
            {
                Debug.LogError($"所选位置在地图外!");
                return null;
            }

            // 2.判断是否合法、边界等
            AStarNode start = NodeS[(int)startPos.x, (int)startPos.y];
            AStarNode end = NodeS[(int)endPos.x, (int)endPos.y];
            if (start.nodeType == AStarNode.AStarNodeType.Stop || end.nodeType == AStarNode.AStarNodeType.Stop)
            {
                Debug.LogError("所选位置不可通行!");
                return null;
            }

            // 3.清空列表的数据
            m_OpenList.Clear();
            m_CloseList.Clear();

            // 4.设置起点的部分信息
            start.father = null;
            start.f = 0;
            start.g = 0;
            start.h = 0;

            // 5.直接将起点加入到关闭队列中
            m_CloseList.Add(start);

            while (true)
            {
                FindNode(start.x - 1, start.y - 1, 1.4f, start, end);   // 左上
                FindNode(start.x, start.y - 1, 1, start, end);            // 上
                FindNode(start.x + 1, start.y - 1, 1.4f, start, end);   // 右上
                FindNode(start.x - 1, start.y, 1, start, end);            // 左
                FindNode(start.x + 1, start.y, 1, start, end);            // 右
                FindNode(start.x - 1, start.y + 1, 1.4f, start, end);   // 左下
                FindNode(start.x, start.y + 1, 1, start, end);           // 下
                FindNode(start.x + 1, start.y + 1, 1.4f, start, end);   // 右下

                // 当开启队列为空时 => 说明周围的点要么不可通行要么不合法
                if (m_OpenList.Count == 0)
                {
                    Debug.Log($"没有出路!");
                    return null;
                }

                // 开启队列中选出最短的路线 => 放入关闭列表中 => 把当前关闭列表中的点设为遍历点，然后循环遍历其周围八个点 => 从开启列表中移除这个点 => 判断是否到达目的地

                // (1)使用sort方法对开启队列进行排序 => 队列的第0个就是最近的点
                m_OpenList.Sort(SortOpenList);

                // (2)将开启列表的第0个点放入到关闭列表中
                m_CloseList.Add(m_OpenList[0]);

                // (3)把当前关闭列表中的点设为遍历点
                start = m_OpenList[0];

                // (4)将该点从开启列表中移除
                m_OpenList.RemoveAt(0);

                // (5)判断是否为终点
                if (start == end)
                {
                    Debug.Log("已经到达目的地!");

                    List<AStarNode> path = new List<AStarNode>();

                    path.Add(end);
                    while (end != null)
                    {
                        path.Add(end.father);
                        end = end.father;
                    }

                    path.Reverse();

                    return path;
                }
            }

        }

        /// <summary>
        /// 数据排序
        /// </summary>
        /// <param name="a">a节点</param>
        /// <param name="b">b节点</param>
        /// <returns></returns>
        private int SortOpenList(AStarNode a, AStarNode b)
        {
            if (a.f > b.f)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }


        private void FindNode(int x, int y, float g, AStarNode father, AStarNode end)
        {
            // 判定边界
            if (x < 0 || x >= mapMaxWidth || y < 0 || y >= mapMaxHeight)
            {
                return;
            }

            // 获取节点
            AStarNode node = NodeS[x, y];
            // 判断阻挡
            if (node == null || node.nodeType == AStarNode.AStarNodeType.Stop || m_CloseList.Contains(node))
            {
                return;
            }

            if (m_OpenList.Contains(node))
            {
                float gThis = father.g + g;
                if (gThis < node.g)
                {
                    node.g = gThis;
                    node.f = node.g + node.h;
                    node.father = father;
                    return;
                }
                else
                {
                    return;
                }
            }

            // 计算f(寻路消耗)

            // 记录同步父值
            node.father = father;

            // 计算g 距离起点的距离 => 父节点距离起点的距离 + 本点距离父节点的距离
            node.g = father.g + g;
            node.h = Mathf.Sqrt(Mathf.Pow(Mathf.Abs(end.x - node.x), 2)+ Mathf.Pow(Mathf.Abs(end.y - node.y),2));
            node.f = node.g + node.h;

            // 如果通过上面的验证则将节点添加到OpenList中
            m_OpenList.Add(node);
        }

        protected override void Init()
        {
            base.Init();

            m_OpenList = new List<AStarNode>();
            m_CloseList = new List<AStarNode>();
        }
    }

    // A星寻路的节点类
    public sealed class AStarNode
    {
        public enum AStarNodeType
        {
            // 不可通行
            Stop = 0,
            // 可通行
            Walk = 1
        }

        // 节点的坐标
        public int x;
        public int y;

        // 寻路的消耗
        public float f;
        // 相对起点的直线距离
        public float g;
        // 相对终点的直线距离
        public float h;

        // 父节点
        public AStarNode father;
        // 节点类型
        public AStarNodeType nodeType;

        // 构造函数初始化数据
        public AStarNode(int x, int y, AStarNodeType type)
        {
            this.x = x;
            this.y = y;
            this.nodeType = type;
        }
    }

}
