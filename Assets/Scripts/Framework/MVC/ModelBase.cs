using System;
using System.Collections;
using System.Collections.Generic;
using Framework;
using UnityEngine;

public class ModelBase 
{
    protected string modelName = String.Empty;

    public virtual string ModelName
    {
        get
        {
            return modelName;
        }
        set
        {
            modelName = value;
        }
    }

    protected virtual void Init()
    {
        Debug.Log($"����ModelBase��Init!");
    }
}
