using System;
using UnityEngine;

public class ViewBase : MonoBehaviour
{
    protected string viewName = String.Empty;
    public virtual string ViewName
    {
        get
        {
            return viewName;
        }
        set
        {
            viewName = value;
        }
    }

    protected virtual void Awake()
    {
        Init();
    }

    protected virtual void Init()
    {
        
    }
}
