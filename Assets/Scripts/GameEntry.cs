using System;
using System.Collections.Generic;
using Framework;
using Framework.Tools;
using GameCore.UI;
using UnityEngine;
using Newtonsoft.Json;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

/*----------------------游戏入口----------------------*/
public class GameEntry : MonoBehaviour
{
    private GameObject m_PlayerObj;

    public string time;

    void Awake()
    {
        // m_PlayerObj = GameObject.FindWithTag("Player");
        // m_PlayerObj.AddComponent<PlayerView>();
    }

    void Start()
    {
        // Debug.Log($"开始加载UIManager => {UIManager.Instance}");
        // UIManager.Instance.PushView<MainViewMgr>(UIName.UIMain);

    }

    void Update()
    {
        
    }

    public async void GetRes()
    {
        var asset = await ABMgr.Instance.LoadResAsync<GameObject>("prefab", "AsunaPrefab");
        Instantiate(asset);
    }

}
