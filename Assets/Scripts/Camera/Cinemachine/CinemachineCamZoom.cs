using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

namespace GameCore.Camera.Cinemachine
{
    /// <summary>
    /// Cinemachine相机相对目标缩放逻辑
    /// </summary>
    public class CinemachineCamZoom : MonoBehaviour
    {
        [SerializeField][Range(0f, 10f)] private float m_DefaultDistance = 6.0f;   // 默认距离
        [SerializeField][Range(0f, 10f)] private float m_MinimumDistance = 1.0f;   // 最小距离
        [SerializeField][Range(0f, 10f)] private float m_MaximumDistance = 6.0f;   // 最大距离

        [SerializeField][Range(0f, 10f)] private float m_Smoothing = 4.0f;         // 延迟
        [SerializeField][Range(0f, 10f)] private float m_ZoomSensitivity = 1.0f;   // 灵敏度

        private float m_CurTargetDistance;  // 当前相对目标的最终距离

        private CinemachineFramingTransposer m_FramingTransposer;
        private CinemachineInputProvider m_InputProvider;

        void Awake()
        {
            m_FramingTransposer = GetComponent<CinemachineVirtualCamera>()
                .GetCinemachineComponent<CinemachineFramingTransposer>();
            m_InputProvider = GetComponent<CinemachineInputProvider>();

            m_CurTargetDistance = m_DefaultDistance;
        }

        void Update()
        {
            Zoom();
        }

        private void Zoom()
        {
            float zoomValue = m_InputProvider.GetAxisValue(2) * m_ZoomSensitivity;

            m_CurTargetDistance = Mathf.Clamp(m_CurTargetDistance + zoomValue, m_MinimumDistance, m_MaximumDistance);
            // m_CurTargetDistance += zoomValue;

            var curDistance = m_FramingTransposer.m_CameraDistance;

            if (Mathf.Abs(curDistance - m_CurTargetDistance) < 0.00025f)
            {
                m_FramingTransposer.m_CameraDistance = m_CurTargetDistance;
                return;
            }

            // 通过插值运算将相机从当前距离逐渐拉近到目标距离
            float lerpedZoomValue = Mathf.Lerp(curDistance, m_CurTargetDistance, m_Smoothing * Time.deltaTime);
            m_FramingTransposer.m_CameraDistance = lerpedZoomValue;
        }
    }
}
