using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.Camera
{
    /**
     * 该脚本处理相机与人物之间有障碍物时的移动逻辑
     * 所以与跟随逻辑的脚本分开写
     */
    public class ProtectCameraFromWallClip : MonoBehaviour
    {
        [SerializeField] private float m_ClipMoveTime = 0.05f;      // 移动耗时
        [SerializeField] private float m_ReturnTime = 0.4f;         // 返回耗时
        [SerializeField] private float m_SphereCastRadius = 0.1f;   // 投射半径
        [SerializeField] private float m_ClosestDistance = 0.5f;    // 相机距离人物的最近距离
        [SerializeField] private bool m_visualiseInEditor;          // 在Editor中能否看到射线(Scene)
        [SerializeField] private string m_DontClipTag = "Player";   // 忽略检测的Tag
        public bool Protecting { get; private set; }                // 是否在规避障碍物

        private Transform m_Cam;                    // 相机的T
        private Transform m_Pivot;                  // 枢纽的T
        private float m_OriginalDist;               // 移动前的原始距离
        private float m_MoveVelocity;               // 移动的速度
        private float m_CurrentDist;                // 移动后的距离
        private Ray m_Ray = new Ray();              // 用于检测遮挡的涉嫌
        private RaycastHit[] m_RayCastHits;         // 缓存射线打到的物体
        private RayHitComparer m_RayHitComparer;    // 射线碰撞结果比较器

        protected void Start()
        {
            // 初始化变量，缓存部分距离
            m_Cam = GetComponentInChildren<UnityEngine.Camera>().transform;
            m_Pivot = m_Cam.parent;
            m_OriginalDist = m_Cam.localPosition.magnitude;
            m_CurrentDist = m_OriginalDist;

            // 申请一个对比器
            m_RayHitComparer = new RayHitComparer();

        }

        // 相机的射线检测移动应该在最后更新
        protected void LateUpdate()
        {
            // 初始化目标距离
            var targetDist = m_OriginalDist;

            // 射线的出发点，每帧更新
            m_Ray.origin = m_Pivot.position;
            // 射线的方向，每帧更新
            m_Ray.direction = -m_Pivot.forward;

            // 射线的原点以一定半径的球形范围内检测碰撞体
            var cols = Physics.OverlapSphere(m_Ray.origin, m_SphereCastRadius);
            var initialIntersect = false;
            var hitSomething = false;

            // 检查射线原点是否在某个物体里面
            // 因为射线原点在某个物体里面的时候无法检RayCast到该物体
            for (int i = 0; i < cols.Length; i++)
            {
                if ((!cols[i].isTrigger) && 
                    !(cols[i].attachedRigidbody != null && cols[i].attachedRigidbody.CompareTag(m_DontClipTag)))
                {
                    // 如果不是触发器并且没有刚体或者有刚体但tag不是player
                    initialIntersect = true;
                    break;
                }
            }

            if (initialIntersect)
            {
                // 如果球体范围内有碰撞体
                // 将射线的起点沿相机到枢纽的线段再延长一个检测球半径的距离
                // 这样可以防止射线的起点被包含在一个碰撞体里面，射线检测不到这个物体导致无法缩进摄像机的距离
                // 例如人物穿过一个狭窄的山洞时相机的缩进
                m_Ray.origin += m_Pivot.forward * m_SphereCastRadius;
                
                m_RayCastHits = Physics.RaycastAll(m_Ray, m_OriginalDist - m_SphereCastRadius);
                Debug.Log($"球体内有物体");
                Debug.DrawRay(m_Ray.origin, -m_Pivot.forward*(m_Cam.transform.position - m_Pivot.transform.position).magnitude, Color.green);
            }
            else
            {
                // 如果球体范围内没有碰撞体
                m_RayCastHits = Physics.RaycastAll(m_Ray, m_OriginalDist);
            }

            // 对击中的物体根据距离进行排序
            Array.Sort(m_RayCastHits, m_RayHitComparer);

            // 缓存最近的距离
            float nearest = Mathf.Infinity;

            // 遍历打到的物体
            for (int i = 0; i < m_RayCastHits.Length; i++)
            {
                var hit = m_RayCastHits[i];
                if (hit.distance < nearest && 
                    !hit.collider.isTrigger &&
                    !(hit.collider.attachedRigidbody != null && hit.collider.attachedRigidbody.CompareTag(m_DontClipTag)))
                {
                    // 如果物体的距离小于缓存的距离并且是碰撞体并且有刚体的话刚体不是tag Player
                    nearest = hit.distance;
                    targetDist = -m_Pivot.InverseTransformPoint(hit.point).z;
                    hitSomething = true;
                }
            }

            if (hitSomething)
            {
                // 有障碍物时画出射线
                Debug.DrawRay(m_Ray.origin, -m_Pivot.forward*(targetDist + m_SphereCastRadius), Color.red);
            }

            Protecting = hitSomething;
            m_CurrentDist = Mathf.SmoothDamp(m_CurrentDist, targetDist, ref m_MoveVelocity, 
                m_CurrentDist > targetDist ? m_ClipMoveTime : m_ReturnTime);
            // 以防超限
            m_CurrentDist = Mathf.Clamp(m_CurrentDist, m_ClosestDistance, m_OriginalDist);
            m_Cam.localPosition = -Vector3.forward * m_CurrentDist;
        }
    }

    /**
     * 该类用于实现射线的比较方法
     * 比较两个碰撞结果的距离
     */
    public class RayHitComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return ((RaycastHit) x).distance.CompareTo(((RaycastHit) y).distance);
        }
    }
}
