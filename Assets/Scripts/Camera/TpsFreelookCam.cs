using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.Camera
{
    /**
     * 该脚本通过继承跟随目标基类的子类"基于枢纽的相机平台类"，主要实现FollowTarget函数的逻辑
     */
    public class TpsFreelookCam : PivotBasedCameraRig
    {
        [SerializeField] private float m_MoveSpeed = 1f;                         // 相机平台的移动速度
        [Range(0f, 10f)] [SerializeField] private float m_TurnSpeed = 1.5f;     // 相机平台的旋转速度
        [SerializeField] private float m_TurnSmoothing = 0.0f;                 // 旋转丝滑度
        [SerializeField] private float m_TiltMax = 75f;                       // 围绕枢纽x轴转动的最大值，即俯角
        [SerializeField] private float m_TiltMin = 45f;                      // 围绕枢纽x轴转动的最小值，即仰角
        [SerializeField] private bool m_LockCursor = false;                 // 是否隐藏并锁定鼠标光标在正中央
        [SerializeField] private bool m_VerticalAutoReturn = false;        // 垂直轴旋转是否自动回正

        private float m_LookAngle;  // 平台的环视角度
        private float m_TiltAngle;  // 平台的俯仰角
        private const float mc_LookDistance = 100f;    // 角色的look目标与枢纽前方向的距离
        private Vector3 m_PivotEulers;
        private Quaternion m_PivotTargetRot;
        private Quaternion m_TransformTargetRot;

        protected override void Awake()
        {
            // 做一些初始化工作
            base.Awake();
            // 是否锁定光标
            Cursor.lockState = m_LockCursor ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !m_LockCursor;
            
            // 获取枢纽的欧拉角值，主要缓存枢纽的y轴与z轴的旋转量，以防其他轴旋转了
            m_PivotEulers = m_Pivot.rotation.eulerAngles;
            // 获取枢纽的本地四元数(旋转值)
            m_PivotTargetRot = m_Pivot.transform.localRotation;
            // 获取平台根的本地四元数(旋转值)
            m_TransformTargetRot = transform.localRotation;
            Debug.Log($"Awake: 初始化光标锁定与缓存相机平台各节点的Rotation");
        }

        protected void Update()
        {
            Debug.DrawRay(m_Cam.transform.position, m_Cam.forward * (m_Cam.transform.position - m_Pivot.transform.position).magnitude, Color.black);
            // 每帧处理旋转与运动
            HandleRotationMovement();

            // 点击鼠标左键时锁定并隐藏光标
            if (m_LockCursor && Input.GetMouseButtonUp(0))
            {
                Cursor.lockState = CursorLockMode.Locked;
                Cursor.visible = false;
            }

        }

        protected void OnDisable()
        {
            // 暂停游戏的时候解锁光标
            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
        
        // 该方法已经被基类的Update函数调用了，此处只实现方法体所以不需要再调用
        // 抽象方法的作用体现，子类实现方法体，父类也可以调用
        /**
         * 跟随目标(平台根跟随)
         */
        protected override void FollowTarget(float deltaTime)
        {
            if (m_Target == null)
            {
                return;
            }

            // rig向目标位置移动
            // 通过线性插值去更新平台根的position
            transform.position = Vector3.Lerp(transform.position, m_Target.position, deltaTime * m_MoveSpeed);
        }

        /**
         * 围绕目标旋转
         */
        private void HandleRotationMovement()
        {
            // 环视的旋转是根节点，俯仰视角旋转是枢纽节点

            if (Time.timeScale < float.Epsilon)
            {
                // 暂停游戏时timeScale为0，为0时所有基于帧率的功能都被暂停
                // 暂停游戏时不再处理旋转
                return;
            }

            // todo：把标准pc端和移动端的输入响应都封装一下
            var x = Input.GetAxis("Mouse X");
            var y = Input.GetAxis("Mouse Y");

            // 根据水平输入量和旋转速度调整LookAngle(看向的方向角度--本地坐标系(其实也是世界坐标系))
            m_LookAngle += x * m_TurnSpeed;

            // 该四元数的旋转值是用到平台根节点的
            m_TransformTargetRot = Quaternion.Euler(0f, m_LookAngle, 0f);

            // 对于垂直方向的视角旋转，需要根据使用的设备做出不同的行为：
            if (m_VerticalAutoReturn)
            {
                // 如果自动回正垂直方向的视角
                
                // 使用移动设备时垂直输入直接映射为旋转值(即输入量范围不在-1到1之间，而是真实值)，因此停止移动视角时应当自动回正
                // 其实也可以做一个系数去决定转动速度，0~1这样
                // 需要判断正负，就算两个最值不对成，因为需要归零
                // 如何自动归零，当不再有垂直方向输入时y为0，那么插值的结果便一直为0，从而达到归零回正的效果0
                m_TiltAngle = y > 0 ? Mathf.Lerp(0, -m_TiltMin, y) : Mathf.Lerp(0, m_TiltMax, -y);
            }
            else
            {
                // 如果不需要自动回正
                // pc设备的鼠标的输入值区间在-1~1之间，所以需要乘上旋转速度
                // 其实这里的旋转速度更像是灵敏度
                m_TiltAngle -= y*m_TurnSpeed;
                // 需要判定界限
                m_TiltAngle = Mathf.Clamp(m_TiltAngle, -m_TiltMin, m_TiltMax);
            }

            // 该四元数是运用到枢纽节点的
            m_PivotTargetRot = Quaternion.Euler(m_TiltAngle, m_PivotEulers.y, m_PivotEulers.z);

            if (m_TurnSmoothing > 0)
            {
                // m_TurningSmoothing * Time.deltaTime => 相当于转动到目标值的速度

                // 枢纽节点进行俯仰视角的旋转，是相对于根节点的本地旋转
                m_Pivot.localRotation = Quaternion.Slerp(m_Pivot.localRotation, m_PivotTargetRot,
                    m_TurnSmoothing * Time.deltaTime);

                // 根节点进行环视旋转
                transform.localRotation = Quaternion.Slerp(transform.localRotation, m_TransformTargetRot,
                    m_TurnSmoothing * Time.deltaTime);
            }
            else
            {
                // 不丝滑就直接转
                m_Pivot.localRotation = m_PivotTargetRot;
                transform.localRotation = m_TransformTargetRot;
            }
        }
    }

}
