using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#pragma warning disable 649
namespace GameCore.Camera
{
    /**
     * 跟随目标基类
     * 该类只会决定怎样更新跟随逻辑函数，不会处理视角旋转与目标跟随的逻辑
     */
    public abstract class AbstractTargetFollower : MonoBehaviour
    {
        public enum UpdateType // 允许在哪个Update函数中更新
        {
            FixedUpdate = 0, // 在fixedUpdate中更新，为了追踪rigidBody
            LateUpdate = 1, // 在LateUpdate中更新，为了追踪在Update中更新运动的物体(一般如此)
            ManualUpdate = 2 // 人为更新
        }

        [SerializeField] protected Transform m_Target;  // 要跟踪的目标
        [SerializeField] private bool m_AutoTargetPlayer = true;    // 平台是否自动跟踪目标
        [SerializeField] private UpdateType m_UpdataType;   // 要放在哪里更新

        protected Rigidbody m_TargetRigidbody;

        public Transform Target
        {
            get { return m_Target; }
        }

        protected virtual void Start()
        {
            // 如果使用自动跟踪，寻找tag为"Player"的物体
            // 任何继承该类的派生类如果重写了Start方法的话需要调用base.Start()以执行下面的操作
            // 不重写的话则会直接调用该基类Start函数
            if (m_AutoTargetPlayer)
            {
                FindAndTargetPlayer();
            }

            if (m_Target == null) return;
            m_TargetRigidbody = m_Target.GetComponent<Rigidbody>();
            Debug.Log($"Start: 目标跟随逻辑基类初始化完成; 找到目标的rigidBody:{m_TargetRigidbody}", m_TargetRigidbody);
        }

        private void FixedUpdate()
        {
            // 如果UpdateType选择了FixedUpdate或者在自动模式，则在此更新运动
            // 如果目标有rigidBody且不是在运动学模式
            if (m_UpdataType == UpdateType.FixedUpdate)
            {
                if (m_AutoTargetPlayer && (m_Target == null || !m_Target.gameObject.activeSelf))
                {
                    // 双保险?
                    FindAndTargetPlayer();
                }

                FollowTarget(Time.deltaTime);
            }
        }
        
        private void LateUpdate()
        {
            // 如果UpdateType选择了LateUpdate，则在此更新运动
            // 如果目标没有rigidBody或者有rigidBody但是在运动学模式上
            if (m_UpdataType == UpdateType.LateUpdate)
            {
                if (m_AutoTargetPlayer && (m_Target == null || !m_Target.gameObject.activeSelf))
                {
                    FindAndTargetPlayer();
                }

                FollowTarget(Time.deltaTime);
            }
        }

        // 供外部调用
        public void ManualUpdate()
        {
            // 如果UpdateType选择了人为更新或者在自动模式，则再次更新运动
            // 应该不会用到
            if (m_UpdataType == UpdateType.ManualUpdate)
            {
                if (m_AutoTargetPlayer && (m_Target == null || !m_Target.gameObject.activeSelf))
                {
                    FindAndTargetPlayer();
                }

                FollowTarget(Time.deltaTime);
            }
        }

        protected abstract void FollowTarget(float deltaTime);

        public void FindAndTargetPlayer()
        {
            // 如果还没有分配目标的话就自动追踪tag为Player的物体
            var target = GameObject.FindGameObjectWithTag("Player");
            if (target)
            {
                SetTarget(target.transform);
            }
        }

        public virtual void SetTarget(Transform targetTransform)
        {
            m_Target = targetTransform;
            try
            {
                // 以防万一跳过了start方法中的获取rigidBody
                m_TargetRigidbody = m_Target.GetComponent<Rigidbody>();
            }
            catch (Exception e)
            {
                Debug.LogError($"target中没有rigidBody组件; {e}");
            }
        }
    }
}

