using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.Camera
{
    public abstract class PivotBasedCameraRig : AbstractTargetFollower
    {
        // 这个类是用于放在相机平台(预制体)的根物体上的，
        // 相机平台应该设置为包含三层物体的预制体：
        // CameraRig => Pivot => Camera
        // 

        protected Transform m_Cam; // camera的transform引用
        protected Transform m_Pivot; // camera所围绕的枢纽点的transform引用，ps：该点肯定是跟着玩家的
        protected Vector3 m_LastTargetPosition;

        protected virtual void Awake()
        {
            // 在层次结构列表中找到camera并获取枢纽
            // 脚本挂载的物体是平台的根物体，不是相机围绕旋转的枢纽
            m_Cam = GetComponentInChildren<UnityEngine.Camera>().transform;
            m_Pivot = m_Cam.parent;
            Debug.Log("Awake: 寻找相机和其围绕的枢纽点的T");
        }

        protected override void Start()
        {
            // base.Start();

        }
    }
}
