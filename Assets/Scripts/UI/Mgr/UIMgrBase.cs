using System.Collections;
using System.Collections.Generic;
using Framework.Extensions;
using UnityEngine;

namespace GameCore.UI
{
    public abstract class UIMgrBase : ControllerBase
    {
        protected UILayer viewLayer;
        protected string viewName;
        protected GameObject viewObj;
        protected UIViewBase view;

        public UILayer ViewLayer
        {
            get => viewLayer;
        }
        public string ViewName
        {
            get => viewName;
        }
        public GameObject ViewObj
        {
            get => viewObj;
        }
        public UIViewBase View
        {
            get => view;
        }

        public virtual void Init(UILayer layer, string name, GameObject obj)
        {
            viewLayer = layer;
            viewName = name;
            viewObj = obj;
            view = viewObj.GetComponent<UIViewBase>();
            if (view == null)
            {
                Debug.LogError($"获取UIViewBase脚本组件失败，请检查预制体上时候挂载！");
            }

        }

        public virtual void OnEnter()
        {
        }

        public virtual void OnPause()
        {
        }

        public virtual void OnResume()
        {
        }

        public virtual void OnExit()
        {
        }

        public virtual void OnRefresh()
        {
        }

    }
}
