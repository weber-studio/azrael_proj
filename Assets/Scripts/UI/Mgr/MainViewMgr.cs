using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.UI
{
    public class MainViewMgr : UIMgrBase
    {
        public override void OnEnter()
        {
            view.btnConfirm.onClick.AddListener(() =>
            {
                UIManager.Instance.PushView<InventoryViewMgr>(UIName.UIInventory);
            });
        }
    }
}

