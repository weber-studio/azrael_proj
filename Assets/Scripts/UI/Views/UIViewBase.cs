using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace GameCore.UI
{
    public class UIViewBase : ViewBase
    {
        public Button btn1;
        public Button btn2;
        public Button btnConfirm;
        public Button btnClose;
        public List<Button> btns;
        public Text txtTitle1;
        public Text txtTitle2;
        public Text txt1;
        public Text txt2;
        public List<Text> txts;
        public Image img1;
        public Image img2;
        public Image imgBg;
        public Image imgOther;
        public List<Image> imgs;
    }
}
