using System;
using System.Collections;
using System.Collections.Generic;
using Framework;
using Framework.Extensions;
using UnityEditor;
using UnityEngine;
using Object = UnityEngine.Object;

namespace GameCore.UI
{
    public partial class UIManager : MonoSingleton<UIManager>
    {
        private Dictionary<string, string> m_DicViewPaths;  // 所有UI面板预制体的路径 || key：viewName || value：路径（也可以是该预制体所在的AB包的路径）
        private Dictionary<string, UILayer> m_DicViewLayer;

        private Dictionary<string, UIMgrBase> m_DicViewLoaded;  // 缓存已经加载过的UIMgr类的字典，多态

        private Stack<UIMgrBase> m_StackViewOpened;      // 栈式存储打开过的面板，先进后出

        private Dictionary<UILayer, Transform> m_DicLayer;      // 存储UI层级节点，方便设置父节点
        private Dictionary<UILayer, string> m_DicLayerName;     // 存储UI层级对应的名称，用于加载UI层级节点时命名

        private RectTransform m_CanvasT;     // Canvas节点

        private readonly string m_UIPathData_Path = @"Assets\Scripts\UI\Data\UIPathData.asset";

        protected override void Init()
        {
            base.Init();
            InitSelf();

            InitPath();
            InitLayer();

            LoadLayer();

        }

        /// <summary>
        /// 创建对应的层级节点并放入到Canvas中
        /// </summary>
        private void LoadLayer()
        {
            m_DicLayer = new Dictionary<UILayer, Transform>();

            for (int i = 0; i < m_DicLayerName.Count; i++)
            {
                var index = i;
                Debug.Log($"Layer:{(UILayer)index}");
                GameObject layer = new GameObject(m_DicLayerName[(UILayer)index]);
                layer.transform.SetParent(m_CanvasT);
                m_DicLayer.Add((UILayer)index, layer.transform);
            }
        }

        /// <summary>
        /// 内部调用，获取dicPanel中缓存的UIBase实例，如果为空则实例化并缓存
        /// </summary>
        /// <typeparam name="T">获取的UI的实际类型</typeparam>
        /// <param name="viewName">获取的名字</param>
        /// <returns></returns>
        private T GetView<T>(string viewName) where T : UIMgrBase
        {
            // 尝试一下使用新一点的语法 XD
            m_DicViewLoaded ??= new Dictionary<string, UIMgrBase>();

            UIMgrBase panelMgr = null;

            // var typeName = string.Concat(viewName, "Mgr");    // 提前缓存类型名字，以防后续需要实例化类的时候使用

            if (!m_DicViewLoaded.ContainsKey(viewName))
            {
                if (!m_DicViewPaths.ContainsKey(viewName))
                {
                    throw new Exception($"路径数据字典中不存在key为“{viewName}”的路径数据，请检查！");
                }

                var path = string.Concat(m_DicViewPaths[viewName], ".prefab");
                Debug.Log($"预制体的加载路径：{path}");
#if UNITY_EDITOR
                var obj = AssetDatabase.LoadAssetAtPath<GameObject>(path);
#else
                // 这里同样要使用AB包的方式去加载界面的预制体 todo：补上AB包加载逻辑
#endif
                if (obj == null)
                {
                    throw new Exception($"加载的预制体为空，请检查路径！");
                }

                var clone = GameObject.Instantiate(obj, m_CanvasT, false);

                // var type = Type.GetType(typeName);
                // panelMgr = Activator.CreateInstance(type) as UIMgrBase;

                panelMgr = Activator.CreateInstance<T>();

                if (panelMgr == null)
                {
                    throw new Exception($"加载的UIMgr实例为空，请检查！");
                }

                var layer = m_DicViewLayer[viewName];  // 从字典中获取该view的layer层级

                clone.transform.SetParent(m_DicLayer[layer]);
                panelMgr.Init(layer, viewName, clone);  // 初始化面板部分数据

                m_DicViewLoaded.Add(viewName, panelMgr);
            }
            else
            {
                panelMgr = m_DicViewLoaded[viewName];
            }

            return panelMgr as T;
        }
        
        /// <summary>
        /// 打开界面
        /// </summary>
        /// <typeparam name="T">界面的实际Mgr类型</typeparam>
        /// <param name="viewName">界面的名称</param>
        public void PushView<T>(string viewName) where T : UIMgrBase
        {
            if (m_StackViewOpened == null)
            {
                m_StackViewOpened = new Stack<UIMgrBase>();
            }

            T viewMgr = GetView<T>(viewName);

            // 如果栈中有界面则要根据当前准备打开界面的layer对其进行一些处理
            if (m_StackViewOpened.Count > 0)
            {
                var curView = m_StackViewOpened.Peek();
                if (curView == viewMgr)
                {
                    // 要打开的界面和当前显示的界面一样
                    Debug.LogWarning($"要打开的界面与当前正在显示的界面一致，无需重复打开!");
                    return;
                }

                switch (viewMgr.ViewLayer)
                {
                    case UILayer.Pop:
                        curView.OnPause();
                        break;
                    case UILayer.Top:
                        break;
                    default:
                        curView.OnExit();
                        curView.ViewObj.SetActiveWhenNeeded(false);
                        // 当打开的界面是全屏覆盖类型的界面（HUD也算）时，就要把弹窗类型和飘窗类型的界面都退栈
                        // 即打开一个新的全屏界面时就要把之前打开过的小窗体都disable并退栈，大窗体则仅关闭
                        while ((int)curView.ViewLayer > 2)
                        {
                            m_StackViewOpened.Pop();
                            curView = m_StackViewOpened.Peek();
                            curView.OnExit();
                            curView.ViewObj.SetActiveWhenNeeded(false);
                        }
                        break;
                }
            }

            m_StackViewOpened.Push(viewMgr);
            viewMgr.OnEnter();
            viewMgr.ViewObj.SetActiveWhenNeeded(true);
        }

        /// <summary>
        /// 关闭当前界面
        /// </summary>
        public void PopView()
        {
            if (m_StackViewOpened == null)
            {
                m_StackViewOpened = new Stack<UIMgrBase>();
            }

            if (m_StackViewOpened.Count <= 1)
            {
                Debug.Log($"暂时没有界面可以关闭！");
                return;
            }

            var pop = m_StackViewOpened.Pop();
            pop.OnExit();
            pop.ViewObj.SetActiveWhenNeeded(false);

            m_StackViewOpened.Peek().OnResume();
            m_StackViewOpened.Peek().ViewObj.SetActiveWhenNeeded(true);
        }

        /// <summary>
        /// 获取当前打开的界面
        /// </summary>
        /// <returns></returns>
        public UIMgrBase GetTopView()
        {
            if (m_StackViewOpened.Count <= 0)
            {
                return null;
            }

            return m_StackViewOpened.Peek();
        }
    }

    public partial class UIManager
    {
        // 初始化自身和Canvas
        private void InitSelf()
        {
            gameObject.name = $"UI_Manager_Singleton";

            m_CanvasT = GameObject.Find("Canvas").GetComponent<RectTransform>();
            /*m_CanvasT = new GameObject("Canvas").AddComponent<RectTransform>();
            var canvas = m_CanvasT.gameObject.AddComponent<Canvas>();
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;
            m_CanvasT.gameObject.AddComponent<CanvasScaler>();
            m_CanvasT.gameObject.AddComponent<GraphicRaycaster>(); */ // 手动创建Canvas

            Instance.transform.SetParent(m_CanvasT);
            var rect = gameObject.AddComponent<RectTransform>();
            rect.anchorMin = new Vector2(0, 0);
            rect.anchorMax = new Vector2(1, 1);
            rect.anchoredPosition = new Vector2(0, 0);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, m_CanvasT.sizeDelta.x);
            rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, m_CanvasT.sizeDelta.y);

            // 一直使用同一个Canvas
            DontDestroyOnLoad(m_CanvasT);
        }

        /// <summary>
        /// 初始化UI界面的加载路径
        /// </summary>
        private void InitPath()
        {
#if UNITY_EDITOR
            var pathData = AssetDatabase.LoadAssetAtPath<UIPathData>(m_UIPathData_Path);
#else
            // 打包之后应该使用AssetBundle记载 => todo: 补上从AB包加载的代码!
#endif
            if (pathData == null)
            {
                throw new Exception($"加载的UI路径数据为空，请检查加载路径m_UIPathData_Path是否正确！");
            }

            m_DicViewPaths = pathData.GetUIDicNamePath();
            if (m_DicViewPaths == null)
            {
                throw new Exception($"UI路径数据转换为字典时为空，请检查！");
            }

            m_DicViewLayer = pathData.GetUIDicNameLayer();
            if (m_DicViewLayer == null)
            {
                throw new Exception($"UI层级数据转换为字典时为空，请检查！");
            }

            // todo: 加载完之后直接卸载为什么会崩溃？而延迟卸载却不会？
            StartCoroutine(DelayedUnLoadAssets(pathData, 1.0f));
        }

        /// <summary>
        /// 延迟卸载资源(暂时)
        /// </summary>
        /// <param name="asset"></param>
        /// <param name="delay"></param>
        /// <returns></returns>
        private IEnumerator DelayedUnLoadAssets(Object asset, float delay)
        {
            if (asset == null)
            {
                yield break;
            }

            yield return new WaitForSeconds(delay);
            Resources.UnloadAsset(asset);
            Debug.Log($"已经卸载资源!");
        }

        /// <summary>
        /// 初始化UI界面的层级节点
        /// </summary>
        private void InitLayer()
        {
            m_DicLayerName = new Dictionary<UILayer, string>();
            m_DicLayerName.Add(UILayer.BackGround, "BackGround");
            m_DicLayerName.Add(UILayer.Default, "Default");
            m_DicLayerName.Add(UILayer.Hud, "Hud");
            m_DicLayerName.Add(UILayer.Pop, "Pop");
            m_DicLayerName.Add(UILayer.Top, "Top");
        }
    }

    public partial class UIManager
    {
        private UIMgrBase m_LastDefaultView;
    }
}
