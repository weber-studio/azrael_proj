using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameCore.UI
{
    // 修改这里的数据同时也要在 Assets\Scripts\UI 下的UIPathData配置文件中修改响应的项
    public class UIName
    {
        public const string UIMain = "MainView";
        public const string UIInventory = "InventoryView";
        public const string UIShop = "ShopView";
        public const string UIQuest = "QuestView";
        public const string UIEquipment = "EquipmentView";
        public const string UISkill = "SkillView";
    }

    public enum UILayer
    {
        BackGround, // 背景层
        Default,    // 默认层
        Hud,        // HUD
        Pop,        // 弹窗
        Top         // 顶层悬浮等
    }
}
