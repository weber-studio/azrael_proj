using System.Collections.Generic;
using UnityEngine;

namespace GameCore.UI
{
    /// <summary>
    /// 使用ScriptableObject的实例去写配置文件的方式
    /// 修改完该脚本后对应的实例也会自动更新，但会把之前已经填上去的数据记录都删除掉
    /// todo: 有无除了xml、excel、json以及本so的方式外更加好的配置文件方式？
    /// </summary>
    [CreateAssetMenu(fileName = "UIPathData", menuName = "UI_Data/UI_PathData", order = 0)]
    public class UIPathData : ScriptableObject
    {
        public List<string> viewNames;

        public List<string> viewPaths;

        public List<UILayer> viewLayers;

        [SerializeField] public Dictionary<string, string> testDic;

        public Dictionary<string, string> GetUIDicNamePath()
        {
            if (viewNames == null || viewPaths == null)
            {
                Debug.LogError($"UI加载路径数据存在空引用，请检查！", this);
                return null;
            }

            if (viewNames.Count != viewPaths.Count)
            {
                Debug.LogError($"数据数量不匹配，请检查！", this);
                return null;
            }

            Dictionary<string, string> dic = new Dictionary<string, string>();

            for (int i = 0; i < viewNames.Count; i++)
            {
                var index = i;
                dic.Add(viewNames[index], viewPaths[index]);
            }

            return dic;
        }

        public Dictionary<string, UILayer> GetUIDicNameLayer()
        {
            if (viewNames == null || viewLayers == null)
            {
                Debug.LogError($"UI层级数据存在空引用，请检查！", this);
                return null;
            }

            if (viewNames.Count != viewLayers.Count)
            {
                Debug.LogError($"数据数量不匹配，请检查！", this);
                return null;
            }

            Dictionary<string, UILayer> dic = new Dictionary<string, UILayer>();

            for (int i = 0; i < viewNames.Count; i++)
            {
                var index = i;
                dic.Add(viewNames[index], viewLayers[index]);
            }

            return dic;
        }
    }

}
