using Framework;
using GameCore.InputSystem;
using UnityEngine;

[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider), typeof(PlayerInput))]
public class PlayerView : ViewBase
{
    /*----------------------Components----------------------*/
    public Rigidbody PlayerRigidbody { get; private set; }
    public CapsuleCollider PlayerCollider { get; private set; }
    /// <summary>
    /// 动画状态机
    /// </summary>
    public Animator Animator { get; private set; }


    /*----------------------References----------------------*/
    /// <summary>
    /// 主摄像机的Transform引用
    /// </summary>
    public Transform MainCameraTrans { get; private set; }
    /// <summary>
    /// 玩家运动状态机
    /// </summary>
    private PlayerMovementStateMachine m_MovementStateMachine;
    /// <summary>
    /// 玩家输入类
    /// </summary>
    public PlayerInput Input { get; private set; }


    /*----------------------Methods----------------------*/
    protected override void Init()
    {
        this.ViewName = $"PlayerView";

        PlayerRigidbody = GetComponent<Rigidbody>();
        PlayerCollider = GetComponent<CapsuleCollider>();
        Animator = GetComponent<Animator>();
        Input = GetComponent<PlayerInput>();

        // Cinemachine控制着主摄像头，因此可以安全获取主摄像头的数据
        // Unity2020.2之前，每次调用Camera.main都会进行Tag搜索，此处缓存以节省性能
        MainCameraTrans = Camera.main.transform;    

        m_MovementStateMachine = new PlayerMovementStateMachine(this);
    }

    void Start()
    {
        m_MovementStateMachine.ChangeState(m_MovementStateMachine.idlingState);
    }

    void Update()
    {
        // 状态机这里要先处理完输入再执行更新
        m_MovementStateMachine.HandleInput();
        m_MovementStateMachine.Update();
    }

    void FixedUpdate()
    {
        m_MovementStateMachine.PhysicsUpdate();
    }
}
