using UnityEngine;
using UnityEngine.InputSystem;

namespace GameCore.InputSystem
{
    /// <summary>
    /// 玩家输入类
    /// </summary>
    public class PlayerInput : MonoBehaviour
    {
        /// <summary>
        /// Player Action Asset
        /// </summary>
        public PlayerInputActions InputActions { get; private set; }    // 玩家输入实例
        
        /// <summary>
        /// Action Map
        /// </summary>
        public PlayerInputActions.PlayerActions PlayerActions { get; private set; } // Action Map

        void Awake()
        {
            InputActions = new PlayerInputActions();
            PlayerActions = InputActions.Player;
        }

        void OnEnable()
        {
            InputActions.Enable();
            PlayerActions.Enable();
        }

        void OnDisable()
        {
            InputActions.Disable();
            PlayerActions.Disable();
        }

        public void EnablePlayerInput()
        {
            PlayerActions.Enable();
        }

        public void DisablePlayerInput()
        {
            PlayerActions.Disable();
        }
    }
}
