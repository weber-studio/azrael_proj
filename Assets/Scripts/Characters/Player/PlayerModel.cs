using System;
using System.Collections;
using UnityEngine;

[Serializable]
public class PlayerModel : ModelBase
{
    public int m_HealthPoint;

    private float m_MoveSpeedMultiple = 1.0f;
    public float MoveSpeedMultiple
    {
        get { return m_MoveSpeedMultiple; }
        set
        {
            if (value > 1.5f)
            {
                m_MoveSpeedMultiple = 1.5f;
                return;
            }

            if (value < 0.1f)
            {
                m_MoveSpeedMultiple = 0.1f;
                return;
            }

            m_MoveSpeedMultiple = value;
        }
    }

    private float m_JumpForceMultiple = 1.0f;

    public float JumpForceMultiple
    {
        get { return m_JumpForceMultiple; }
        set
        {
            if (value > 1.5f)
            {
                m_JumpForceMultiple = 1.5f;
                return;
            }

            if (value < 0.25f)
            {
                m_JumpForceMultiple = 0.25f;
                return;
            }

            m_JumpForceMultiple = value;
        }
    }

    protected override void Init()
    {
        base.Init();
        Debug.Log($"����PlayerModel��Init!");
    }
}
