using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerJumpingState : PlayerMovementState
{
    private float m_JumpUpVelocity = 0;
    private float m_JumpUpForce = 5.0f;

    public PlayerJumpingState(PlayerMovementStateMachine playerMovementStateMachine) : base(playerMovementStateMachine)
    {
        stateMachine = playerMovementStateMachine;
    }

    public override void OnEnter()
    {
        base.OnEnter();
        stateMachine.Player.PlayerRigidbody.AddForce(m_JumpUpForce * Vector3.up, ForceMode.Impulse);
    }

    public override void PhysicsUpdate()
    {
        OnJumping();
    }

    #region Main Methods

    private void OnJumping()
    {
        var vc = stateMachine.Player.PlayerRigidbody.velocity;
        m_JumpUpVelocity = vc.y;

        // 当向上的速度为0时开始进入下落状态
        if (m_JumpUpVelocity < 0.00025f)
        {
            stateMachine.Player.PlayerRigidbody.velocity = new Vector3(vc.x, 0, vc.z);
            stateMachine.ChangeState(stateMachine.fallingState);
        }
    }
    

    #endregion
}
