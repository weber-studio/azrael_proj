using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFallingState : PlayerMovementState
{
    public PlayerFallingState(PlayerMovementStateMachine playerMovementStateMachine) : base(playerMovementStateMachine)
    {
        stateMachine = playerMovementStateMachine;
    }

    public override void PhysicsUpdate()
    {
        var player = stateMachine.Player;
        var ray = new Ray(player.transform.position, Vector3.down);
        var hitInfo = new RaycastHit();

        if (Physics.Raycast(ray, out hitInfo, 0.1f))
        {
            Debug.Log($"Raycast!");
            if (hitInfo.transform.gameObject.layer == 6)
            {
                stateMachine.ChangeState(stateMachine.idlingState);
            }
        }
    }
}
