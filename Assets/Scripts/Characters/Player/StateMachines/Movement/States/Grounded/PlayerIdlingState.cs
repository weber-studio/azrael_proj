using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerIdlingState : PlayerMovementState
{
    public PlayerIdlingState(PlayerMovementStateMachine playerMovementStateMachine) : base(playerMovementStateMachine)
    {
        stateMachine = playerMovementStateMachine;
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        if(movementInput != Vector2.zero)
        {
            Debug.Log($"moving");
            stateMachine.ChangeState(stateMachine.walkingState);
        }
    }
}
