using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWalkingState : PlayerMovementState
{
    public PlayerWalkingState(PlayerMovementStateMachine playerMovementStateMachine) : base(playerMovementStateMachine)
    {
        stateMachine = playerMovementStateMachine;
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
        if (movementInput == Vector2.zero)
        {
            stateMachine.ChangeState(stateMachine.idlingState);
        }
    }
}
