using UnityEngine;

/// <summary>
/// 状态类中运动状态类的基类；
/// 该类本身不参与工作，只是为了组合通用逻辑 >
/// </summary>
public class PlayerMovementState : IState
{
    protected PlayerMovementStateMachine stateMachine;

    protected Vector2 movementInput;
    protected float jumpInput;

    [SerializeField] protected float baseSpeed = 5f;
    [SerializeField] protected float speedModifier = 1f;

    /// <summary>
    /// 当前目标的旋转值;
    /// 如果求出的玩家的新目标旋转值与该值不符合的话则会更新该值为新目标值
    /// </summary>
    protected Vector3 curTargetRotation;
    protected Vector3 timeToReachTargetRotation;
    protected Vector3 dampedTargetRotationCurVelocity;
    protected Vector3 dampedTargetRotationPassedTime;

    public PlayerMovementState(PlayerMovementStateMachine playerMovementStateMachine)
    {
        stateMachine = playerMovementStateMachine;

        InitData();
    }

    private void InitData()
    {
        timeToReachTargetRotation.y = 0.14f;
    }

    #region IState Methods

    public virtual void OnEnter()
    {
        Debug.Log($"Player当前进入的状态：{GetType().Name}");
    }

    public virtual void HandleInput()
    {
        ReadMovementInput();
    }

    public virtual void Update()
    {

    }

    public virtual void PhysicsUpdate()
    {
        Move();
        Jump();
    }

    public virtual void OnExit()
    {
        Debug.Log($"Player当前退出的状态：{GetType().Name}");
    }

    #endregion

    #region Main Methods
    private void ReadMovementInput()
    {
        // 读取玩家的移动输入
        movementInput = stateMachine.Player.Input.PlayerActions.Movement.ReadValue<Vector2>();
        // 读取玩家的跳跃输入
        jumpInput = stateMachine.Player.Input.PlayerActions.Jump.ReadValue<float>();

        // 光标隐藏
        var cursorState = stateMachine.Player.Input.PlayerActions.CursorHidding.WasPressedThisFrame();
        if (cursorState)
        {
            // 现在lockedMode自带隐藏？
            Cursor.lockState = Cursor.lockState == CursorLockMode.Locked ? CursorLockMode.None : CursorLockMode.Locked;
            // Cursor.visible = !Cursor.visible;
        }
    }

    /// <summary>
    /// 玩家移动
    /// </summary>
    private void Move()
    {
        if (movementInput == Vector2.zero)
        {
            return;
        }

        Vector3 movementDirection = GetMovementInputDirection();

        // 玩家的旋转要根据运动输入进行，所以放在Move()中判断是否旋转
        // 得出的旋转后的方向也要作为实际移动的方向
        float targetRotationYAngle = Rotate(movementDirection);

        Vector3 targetRotationDirection = GetTargetRotationDirection(targetRotationYAngle);

        float movementSpeed = GetMoveSpeed();

        Vector3 curPlayerHorizontalVelocity = GetPlayerHorizontalVelocity();

        stateMachine.Player.PlayerRigidbody.AddForce(movementSpeed * targetRotationDirection - curPlayerHorizontalVelocity, ForceMode.VelocityChange);
    }

    /// <summary>
    /// 玩家跳跃
    /// </summary>
    private void Jump()
    {
        if (jumpInput < 0.0025f)
        {
            return;
        }

        stateMachine.ChangeState(stateMachine.jumpingState);
    }


    /*----------------------玩家的旋转----------------------*/
    /**
     * 玩家的旋转要考虑玩家的输入以及当前摄像机的角度
     * 玩家人物的正方向的绝对角度值等于玩家输入的角度(通过向量转换得出)加上摄像机当前旋转的角度值
     * 例如：玩家输入正方向并且摄像机旋转了45度 => 玩家输入0度 + 摄像机视角45度 => 玩家人物正方向相对于世界坐标系的角度
     */
    /// <summary>
    /// 根据玩家的输入操作以及当前相机面向的角度得出人物实际应该面向的角度
    /// </summary>
    /// <param name="direction">玩家运动输入的三维向量</param>
    /// <returns></returns>
    private float Rotate(Vector3 direction)
    {
        // 得出新的目标旋转值
        var directionAngle = UpdateTargetRotation(direction);

        // 开始逐帧旋转玩家人物
        RotateTowardsTargetRotation();

        return directionAngle;
    }

    /// <summary>
    /// 将玩家输入转换为角度
    /// </summary>
    /// <param name="direction"></param>
    /// <returns></returns>
    private float GetDirectionAngle(Vector3 direction)
    {
        // 将玩家输入的2维向量（3维）转换为角度值，Z => Y
        // 正方向为0，顺时针
        // 默认坐标轴是以X轴为0度基准轴，而按正常的ATan(z,x)反正切得出的角度值也是基于x轴的
        // 但因为Unity中是以Z轴为正方向(0度基准轴)，所以这时候将z与x的位置对调一下为Atan(x,z)，相当于将坐标轴沿2、4象限对角线翻转
        // 则可以得出以Z轴为正方向(0度基准轴)的向量夹角角度值(顺时针旋转正角度值)
        float directionAngle = Mathf.Atan2(direction.x, direction.z) * Mathf.Rad2Deg;

        // 取正值
        // Mathf.Atan2返回的值的区间为-180~180，所以需要对其转换为0~360的范围区间(负角转正角)
        // 旋转不会总是沿着最短的路径到达目标角度，所以可能会错误的旋转(抽搐旋转)
        if (directionAngle < 0f)
        {
            directionAngle += 360f;
        }

        return directionAngle;
    }

    /// <summary>
    /// 加上摄像机角度
    /// </summary>
    /// <param name="angle"></param>
    /// <returns></returns>
    private float AddCameraRotationToAngle(float angle)
    {
        // 加上摄像机瞄向的方向
        angle += stateMachine.Player.MainCameraTrans.eulerAngles.y;

        // 防止溢出
        angle %= 360f;
        return angle;
    }

    /// <summary>
    /// 更新目标旋转角度以及归零passedTime.y
    /// </summary>
    /// <param name="targetAngle"></param>
    private void UpdateTargetRotationData(float targetAngle)
    {
        curTargetRotation.y = targetAngle;

        dampedTargetRotationPassedTime.y = 0f;
    }

    #endregion



    #region Reuseable Methods

    /*----------------------玩家移动----------------------*/
    /// <summary>
    /// 通过输入获取移动方向
    /// </summary>
    /// <returns></returns>
    protected Vector3 GetMovementInputDirection()
    {
        return new Vector3(movementInput.x, 0f, movementInput.y);
    }

    /// <summary>
    /// 移动速度
    /// </summary>
    /// <returns></returns>
    protected float GetMoveSpeed()
    {
        return baseSpeed * speedModifier;
    }

    protected Vector3 GetPlayerHorizontalVelocity()
    {
        Vector3 playerHorizontalVelocity = stateMachine.Player.PlayerRigidbody.velocity;

        // 剔除掉y轴分量，因为y轴分量的速度不受玩家输入影响
        playerHorizontalVelocity.y = 0f;

        return playerHorizontalVelocity;
    }

    /*----------------------玩家旋转----------------------*/
    protected void RotateTowardsTargetRotation()
    {
        // 获取(更新)当前玩家人物正方向的角度值
        float curYAngle = stateMachine.Player.PlayerRigidbody.rotation.eulerAngles.y;

        // 如果当前玩家人物正方向的角度值与当前目标旋转角度值相同的话则不做旋转
        if (Mathf.Abs(curYAngle - curTargetRotation.y) < 0.00025f)
        {
            return;
        }

        // Mathf.SmoothDampAngle()在功能效果上类似于Mathf.Lerp()，但使用上更加复杂，主要为了获取旋转速度让后面使用
        // 求出这一帧旋转的角度smoothedYAngle
        // smoothTime决定旋转的快慢
        // 每次旋转完都dampedTargetRotationPassedTime.y += Time.deltaTime是为了更新这一帧旋转所用的时间
        // timeToReachTargetRotation.y - dampedTargetRotationPassedTime.y是为了让总的旋转时间控制为timeToReachTargetRotation.y
        float smoothedYAngle = Mathf.SmoothDampAngle(curYAngle, curTargetRotation.y, ref dampedTargetRotationCurVelocity.y, timeToReachTargetRotation.y - dampedTargetRotationPassedTime.y);

        dampedTargetRotationPassedTime.y += Time.deltaTime; // Time.deltaTime在FixedUpdate中调用时会自动返回fixedDeltaTime

        Quaternion targetRotation = Quaternion.Euler(0f, smoothedYAngle, 0f);

        stateMachine.Player.PlayerRigidbody.MoveRotation(targetRotation);
    }

    /// <summary>
    /// 更新玩家的目标旋转角度
    /// </summary>
    /// <param name="direction">玩家运动输入的三维向量</param>
    /// <param name="shouldConsiderCamRotation">是否考虑相机的旋转</param>
    /// <returns></returns>
    protected float UpdateTargetRotation(Vector3 direction, bool shouldConsiderCamRotation = true)
    {
        // 先将玩家输入的三维向量(二维)转换为玩家自身移动的角度值
        var directionAngle = GetDirectionAngle(direction);

        // 再加上摄像机当前旋转的角度值得出玩家实际需要旋转的角度值
        if (shouldConsiderCamRotation)
        {
            directionAngle = AddCameraRotationToAngle(directionAngle);
        }

        if (Mathf.Abs(directionAngle - curTargetRotation.y) > 0.00025f)
        {
            UpdateTargetRotationData(directionAngle);
        }

        return directionAngle;
    }

    protected Vector3 GetTargetRotationDirection(float targetAngle)
    {
        return Quaternion.Euler(0f, targetAngle, 0f) * Vector3.forward;
    }
    #endregion
}
