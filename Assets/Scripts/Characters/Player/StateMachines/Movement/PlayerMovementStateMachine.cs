using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// 玩家运动类状态机
/// </summary>
public partial class PlayerMovementStateMachine : StateMachineBase
{
    // 对player的缓存
    public PlayerView Player { get; }

    public PlayerMovementStateMachine(PlayerView player)
    {
        CachePlayerMovementStates();

        Player = player;
    }

}

/// <summary>
/// 这里是状态缓存的工作
/// </summary>
public partial class PlayerMovementStateMachine
{
    public PlayerIdlingState idlingState { get; set; }
    public PlayerWalkingState walkingState { get; set; }
    public PlayerRunningState runningState { get; set; }
    public PlayerJumpingState jumpingState { get; set; }
    public PlayerFallingState fallingState { get; set; }

    private void CachePlayerMovementStates()
    {
        idlingState = new PlayerIdlingState(this);
        walkingState = new PlayerWalkingState(this);
        runningState = new PlayerRunningState(this);
        jumpingState = new PlayerJumpingState(this);
        fallingState = new PlayerFallingState(this);
    }
}
