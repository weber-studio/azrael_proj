using System.Collections;
using System.Collections.Generic;
using Framework.Extensions;
using MText;
using UnityEngine;
using UnityEngine.InputSystem;

/// <summary>
/// This src is just made for the Btn Events.
/// </summary>
public class T1_Mgr : MonoBehaviour
{
    public GameObject btnsList1;

    public GameObject btnsList2;

    public Modular3DText selectionText;

    void Start()
    {
        btnsList1.SetActiveWhenNeeded(false);
        btnsList2.SetActiveWhenNeeded(false);
    }

    public void OnClickStartBtn()
    {
        if (btnsList1.activeInHierarchy)
        {
            return;
        }

        LeanTween.value(btnsList1, pos =>
        {
            btnsList1.SetActiveWhenNeeded(true);
            btnsList1.transform.localPosition = pos;
        }, btnsList1.transform.localPosition - new Vector3(0, 3, 0), btnsList1.transform.localPosition, .5f);

        btnsList1.GetComponent<PlayerInput>().enabled = true;

        LeanTween.delayedCall(0.5f, () =>
        {
            var startBtn = GameObject.Find("StartBtn").GetComponent<MText_UI_Button>();
            startBtn.interactable = false;
        });
    }

    public void OnToggle()
    {
        Debug.Log($"state1:{btnsList2.activeInHierarchy}");
        btnsList2.SetActiveWhenNeeded(!btnsList2.activeInHierarchy);
        Debug.Log($"state2:{btnsList2.activeInHierarchy}");
        if (btnsList2.activeInHierarchy)
        {
            LeanTween.value(btnsList2, pos =>
            {
                btnsList2.transform.localPosition = pos;
            }, btnsList2.transform.localPosition - new Vector3(0, 3, 0), btnsList2.transform.localPosition, .5f);

            CameraMoveRight();
        }
        else
        {
            CameraMoveLeft();
        }
    }

    public void SetSelection(Modular3DText selection)
    {
        selectionText.Text = selection.Text;
    }

    public void CameraMoveRight()
    {
        var camT = Camera.main.transform;
        LeanTween.value(camT.gameObject, pos =>
        {
            camT.localPosition = pos;
        }, camT.localPosition, camT.localPosition + new Vector3(10, 0, -2), .5f);
    }
    
    public void CameraMoveLeft()
    {
        var camT = Camera.main.transform;
        LeanTween.value(camT.gameObject, pos =>
        {
            camT.localPosition = pos;
        }, camT.localPosition, camT.localPosition + new Vector3(-10, 0, 2), .5f);
    }
}
