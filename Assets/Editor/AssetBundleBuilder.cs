using System.IO;
using UnityEngine;
using UnityEditor;

namespace Framework.AssetBundleTools
{
    public class AssetBundleBuilder
    {
        [MenuItem("AssetBundle/BuildAssetBundle")]
        public static void BuildAssetBundle()
        {
            string OutputDir = Application.streamingAssetsPath + "/AssetBundles";
            if (!Directory.Exists(OutputDir))
            {
                Directory.CreateDirectory(OutputDir);
            }

            var manifest = BuildPipeline.BuildAssetBundles(OutputDir, BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
        }
    }
}

