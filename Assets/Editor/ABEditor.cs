using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

/// <summary>
/// 该打包类是根据资源文件存放的路径结构进行打包的，不是太建议使用。
/// 这里写下来只做按路径打包的参考。
/// </summary>
public class ABEditor
{
    /// <summary>
    /// 需要打包的资源所在的目标文件夹,
    /// 该路径下的所有资源都会被打包。
    /// </summary>
    public static string rootPath = Application.dataPath + "/Res";

    /// <summary>
    /// AB包输出路径
    /// </summary>
#if UNITY_IOS
    public static string outputPath = Application.streamingAssetsPath + "/Ios";
#elif UNITY_ANDROID
    public static string outputPath = Application.streamingAssetsPath + "/Android"
#else
    public static string outputPath = Application.streamingAssetsPath + "/PC";
#endif

    /// <summary>
    /// 记录每个资源所在的AB包文件。
    /// key为资源路径
    /// value为AB包名
    /// </summary>
    public static Dictionary<string, string> asset2BundleDic = new Dictionary<string, string>();

    /// <summary>
    /// 所有需要打包的资源对应的AssetBundleBuild，
    /// 一个文件对应一个AssetBundleBuild。
    /// </summary>
    public static List<AssetBundleBuild> assetBundleBuildList = new List<AssetBundleBuild>();

    /// <summary>
    /// 记录每个Asset资源所依赖的AB包文件。
    /// key为Asset名
    /// value为Asset所依赖的AB包集
    /// </summary>
    public static Dictionary<string, List<string>> asset2DependenciesDic = new Dictionary<string, List<string>>();

    [MenuItem("AssetBundle/Test")]
    public static void Method()
    {
        Directory.Delete(Application.streamingAssetsPath+"/Test/Folder1");
    }

    /// <summary>
    /// 递归打包AB包
    /// </summary>
    [MenuItem("AssetBundle/BuildeAB")]
    public static void BuildAssetBundle()
    {
        Debug.Log($"开始打包！");

        // 打包前将输出目录中的包都清理
        if (Directory.Exists(outputPath))
        {
            Directory.Delete(outputPath, true);
        }

        // 打包所有的资源存放路径下的所有资源
        DirectoryInfo rootDir = new DirectoryInfo(Application.dataPath+"/TestFolder");
        // 获取资源路径下所有的文件夹
        DirectoryInfo[] dirs = rootDir.GetDirectories();
        foreach (var moduleDir in dirs)
        {
            string moduleName = moduleDir.Name;
            assetBundleBuildList.Clear();
            asset2BundleDic.Clear();
            ScanChildDirections(moduleDir);
            AssetDatabase.Refresh();
            string moduleOuputPath = Application.streamingAssetsPath + $"/Test/{moduleName}/" ;
            
            if (Directory.Exists(moduleOuputPath))
            {
                Directory.Delete(moduleOuputPath);
            }
            Directory.CreateDirectory(moduleOuputPath);

            // 按文件夹打包
            BuildPipeline.BuildAssetBundles(moduleOuputPath, assetBundleBuildList.ToArray(),
                BuildAssetBundleOptions.ChunkBasedCompression, EditorUserBuildSettings.activeBuildTarget);

            AssetDatabase.Refresh();
        }

        Debug.Log($"打包结束！");
    }

    /// <summary>
    /// 根据指定的文件夹
    /// 将这个文件夹下所有的一级子文件夹打成一个AB包
    /// 递归这个文件夹下所有的子文件夹
    /// </summary>
    /// <param name="directoryInfo"></param>
    private static void ScanChildDirections(DirectoryInfo directoryInfo)
    {
        if (directoryInfo.Name.EndsWith("CSProject") || directoryInfo.Name.EndsWith(".cs"))
        {
            Debug.Log($"Contain Cs");
            // 若果是脚本则不进行处理
            return;
        }

        ScanCurDirectory(directoryInfo);

        DirectoryInfo[] dirs = directoryInfo.GetDirectories();
        foreach (var dir in dirs)
        {
            ScanChildDirections(dir);
        }
    }

    /// <summary>
    /// 遍历当前路径下的文件，并打包成一个ab包
    /// </summary>
    /// <param name="directoryInfo"></param>
    private static void ScanCurDirectory(DirectoryInfo directoryInfo)
    {
        List<string> assetNameList = new List<string>();

        FileInfo[] fileInfoArr = directoryInfo.GetFiles();

        foreach (var fileInfo in fileInfoArr)
        {
            if (fileInfo.FullName.EndsWith(".meta"))
            {
                continue;
            }

            Debug.Log(
                $"FullName:{fileInfo.FullName} - {Application.dataPath.Length} - {"Assets".Length}");
            string assetName = fileInfo.FullName.Substring(Application.dataPath.Length - "Assets".Length)
                .Replace('\\', '/');
            assetNameList.Add(assetName);
        }

        if (assetNameList.Count > 0)
        {
            string assetBundleName = directoryInfo.FullName.Substring(Application.dataPath.Length + 1)
                .Replace('\\', '_').ToLower();
            AssetBundleBuild build = new AssetBundleBuild();
            build.assetBundleName = assetBundleName;
            build.assetNames = new string[assetNameList.Count];
            for (int i = 0; i < assetNameList.Count; i++)
            {
                build.assetNames[i] = assetNameList[i];
                asset2BundleDic.Add(assetNameList[i], assetBundleName);
            }
            assetBundleBuildList.Add(build);
        }
    }
}
